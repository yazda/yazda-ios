//
//  SettingsViewController.swift
//  yazda-ios
//
//  Created by James Stoup on 6/11/15.
//  Copyright (c) 2015 yazda. All rights reserved.
//

import UIKit
import AFNetworking

class SettingsViewController: BaseViewController {
    @IBOutlet weak var emailAddressText: YazdaTextField!
    @IBOutlet weak var phoneNumberText: YazdaTextField!
    @IBOutlet weak var newsletterSwitch: UISwitch!
    @IBOutlet weak var notificationInviteSwitch: UISwitch!
    @IBOutlet weak var notificationFriendRequestSwitch: UISwitch!

    @IBAction func saveSettings(sender: AnyObject) {
        self.disableActions()
        
        let email = self.emailAddressText.text
        let phone = self.phoneNumberText.text
        let newsletter = self.newsletterSwitch.on
        let notifiesInvite = self.notificationInviteSwitch.on
        let notifiesFriendRequest = self.notificationFriendRequestSwitch.on
        
        client?.account.update(email, phone: phone, newsletter: newsletter, name: nil, location: nil, description: nil, notifiesInvite: notifiesInvite, notifiesFriendRequest: notifiesFriendRequest, viewsMountainBiking: nil, viewsHiking: nil, success: { (obj) -> Void in
            
            self.enableActions()
            self.navigationController?.popViewControllerAnimated(true)
            
            Common.registerForNotifications()
            
            }, failure: { (op: AFHTTPRequestOperation, error: NSError) -> Void in
                if let obj = op.responseObject as? NSDictionary {
                    let messages = obj.objectForKey("errors") as! [String]
                    
                    YazdaAlert(parentView: self, messages: messages, withDelay: Common.Interval)
                }
                self.enableActions()
        })
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.phoneNumberText.delegate = self
        self.emailAddressText.delegate = self
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.disableActions()
        
        client?.account.settings({ (obj: AnyObject!) -> Void in
            let response = obj as! NSDictionary
            let me = obj.objectForKey("user") as! NSDictionary
            
            self.emailAddressText.text = me.objectForKey("email") as! String
            self.phoneNumberText.text = me.objectForKey("phone") as? String
            self.newsletterSwitch.on = me.objectForKey("newsletter") as! Bool
            self.notificationFriendRequestSwitch.on = me.objectForKey("notifies_invite") as! Bool
            self.notificationInviteSwitch.on = me.objectForKey("notifies_friend_request") as! Bool
            
            self.enableActions()
            }, failure: { (op: AFHTTPRequestOperation, error: NSError) -> Void in
                if let obj = op.responseObject as? NSDictionary {
                    let messages = obj.objectForKey("errors") as! [String]
                    
                    YazdaAlert(parentView: self, messages: messages, withDelay: Common.Interval)
                }
                self.enableActions()
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
