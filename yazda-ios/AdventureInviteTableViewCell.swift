//
//  AdventureInviteTableViewCell.swift
//  yazda-ios
//
//  Created by James Stoup on 6/4/15.
//  Copyright (c) 2015 yazda. All rights reserved.
//

import UIKit

class AdventureInviteTableViewCell: UITableViewCell {
    var invite:NSDictionary?
    
    @IBOutlet weak var ownerImage: UIImageView!
    @IBOutlet weak var timeAgoLabel: UILabel!
    @IBOutlet weak var adventureTypeImage: UILabel!
    @IBOutlet weak var adventureTypeLabel: UILabel!
    @IBOutlet weak var adventureName: UILabel!
    @IBOutlet weak var invitedByLabel: UILabel!
    @IBOutlet weak var adventureModifiers: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setInviteData(invite: NSDictionary!) {
        self.invite = invite
        
        let adventure = self.invite!.objectForKey("adventure") as! NSDictionary
        let adventureType = adventure.objectForKey("adventure_type") as! String
        let owner = adventure.objectForKey("owner") as! NSDictionary
        let url = NSURL(string: (owner.objectForKey("profile_image_thumb_url") as! String))
        let ownerName = owner.objectForKey("name") as! String
        let createdAt = Common.toDate(self.invite!.objectForKey("created_at") as! String)
        let startDateTime = Common.toDate(adventure.objectForKey("start_time") as! String)
        let endDateTime = Common.toDate(adventure.objectForKey("end_time") as! String)
        
        BackgroundTask.downloadUserProfileImage(&ownerImage, url: url)
        self.invitedByLabel.text = "Invited by \(ownerName)"
        
        self.adventureModifiers.font = UIFont(name: "icomoon", size: 25)
        self.adventureModifiers.textColor = Common.yazdaIconGray
        self.adventureModifiers.sizeToFit()
        
        if Common.daysBetweenDate(startDateTime, toDateTime: endDateTime) > 1 {
            self.adventureModifiers.text = Common.MULTI_DAY
        }
        
        // Adventure type
        if(adventureType == "mountain_biking")
        {
            self.adventureTypeLabel.text = "Mountain Biking"
            
            self.adventureTypeImage.font = UIFont(name: "FontAwesome", size: 20)
            self.adventureTypeImage.text = Common.MOUNTAIN_BIKING
        } else if(adventureType == "hiking") {
            self.adventureTypeLabel.text = "Hiking"
            
            self.adventureTypeImage.font = UIFont(name: "icomoon", size: 20)
            self.adventureTypeImage.text = Common.HIKING
        }
        
        self.adventureName.text = adventure.objectForKey("name") as? String
        
        self.timeAgoLabel.text = createdAt.timeAgoSinceDate(false)
    }
}
