//
//  ProfileViewController.swift
//  yazda-ios
//
//  Created by James Stoup on 3/15/15.
//  Copyright (c) 2015 yazda. All rights reserved.
//

import UIKit
import AFNetworking
import FontAwesomeIconFactory

class ProfileViewController: BaseViewController {
    var user:NSDictionary = NSDictionary()
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var addFriendButton: YazdaButton!
    @IBOutlet weak var followingButton: UIButton!
    @IBOutlet weak var followersButton: UIButton!
    @IBOutlet weak var adventuresCreatedLabel: UILabel!
    @IBOutlet weak var descriptionText: UITextView!
    @IBOutlet weak var searchForFriends: UIButton!
    
    @IBAction func logout(sender: AnyObject) {
        client!.session.logout()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var factory = NIKFontAwesomeIconFactory.buttonIconFactory()
        factory.size = 15
        self.searchForFriends.setImage(factory.createImageForIcon(NIKFontAwesomeIcon.Search), forState: UIControlState.Normal)
    }
    
    override func viewWillAppear(animated: Bool) {
        getUserProfile()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getUserProfile() {
        var defaults = NSUserDefaults.standardUserDefaults()
        disableActions()
        
        client?.account.me({ (obj) -> Void in
            self.user = (obj as! NSDictionary).objectForKey("user") as! NSDictionary
            
            self.setUserInformation()
            self.enableActions()
            }, failure: { (op: AFHTTPRequestOperation, error: NSError) -> Void in
                self.enableActions()
                //TODO
        })
    }
    
    func setUserInformation() {
        let url = self.user.objectForKey("profile_image_url") as! String
        let imageUrl = NSURL(string: url)
        let followingCount = self.user.objectForKey("following_count") as! Int
        let followersCount = self.user.objectForKey("followers_count") as! Int
        let adventuresCount = self.user.objectForKey("adventures_count") as! Int
        let location = self.user.objectForKey("location") as? String
        
        BackgroundTask.downloadUserProfileImage(&self.profileImage, url: imageUrl!)
        
        self.nameLabel.text = self.user.objectForKey("name") as? String
        self.descriptionText.text = self.user.objectForKey("description") as? String
        self.followingButton.setTitle("\(followingCount) following", forState: UIControlState.Normal)
        self.followersButton.setTitle("\(followersCount) followers", forState: UIControlState.Normal)
        self.adventuresCreatedLabel.text = "\(adventuresCount) adventures created"
        self.locationLabel.text = location
        
        self.descriptionText.textAlignment = NSTextAlignment.Center
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "searchForFriends" {
            let controller = segue.destinationViewController as? InviteFriendsTableViewController
        
            controller!.isSearch = true
            controller!.isInviteFriends = false
            controller!.friends = NSMutableArray()
        } else if let controller = segue.destinationViewController as? InviteFriendsTableViewController {
            controller.profileUserId = user.objectForKey("id") as? Int
            
            if segue.identifier == "showFollowing" {
                controller.showFollowers = false
            } else if segue.identifier == "showFollowers" {
                controller.showFollowers = true
            }
        } else if let controller = segue.destinationViewController as? EditProfileViewController {
            
            controller.user = user
        }
    }
}
