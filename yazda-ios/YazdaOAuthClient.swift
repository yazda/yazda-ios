//
//  YazdaOAuthClient.swift
//  yazda-ios
//
//  Created by James Stoup on 3/12/15.
//  Copyright (c) 2015 yazda. All rights reserved.
//

import Foundation
import AFNetworking
import AFOAuth2Manager

public class YazdaOAuthClient: NSObject {
    let baseURLString:String
    let baseURL:NSURL
    let clientID:String
    let clientSecret:String
    let tokenPath:String
    let authPath:String
    let callbackURL:String
    
    lazy var oauthClient:AFOAuth2Manager? = AFOAuth2Manager(baseURL: self.baseURL, clientID: self.clientID, secret: self.clientSecret)
    lazy var httpClient:AFHTTPRequestOperationManager = AFHTTPRequestOperationManager(baseURL: self.baseURL)
    lazy var reachabilityManager:AFNetworkReachabilityManager = self.oauthClient!.reachabilityManager
    
    lazy var LOGGED_IN_IDENTIFIER:String = self.oauthClient!.serviceProviderIdentifier + "Logged In"
    lazy var LOGGED_OUT_IDENTIFIER:String = self.oauthClient!.serviceProviderIdentifier + "Logged Out"
    
    var creds:AFOAuthCredential?
    var grantType:NSString = ""
    
    override init()
    {
        let settings = Configuration.defaultConfiguration()
        
        baseURLString = settings.settingForKey("base_url")
        baseURL = NSURL(string: baseURLString)!
        clientID = settings.settingForKey("client_key")
        clientSecret = settings.settingForKey("client_secret")
        tokenPath = settings.settingForKey("token_path")
        authPath = settings.settingForKey("auth_path")
        callbackURL = settings.settingForKey("callback_url")
        
        super.init()
        
        var loggedInCred = AFOAuthCredential.retrieveCredentialWithIdentifier(LOGGED_IN_IDENTIFIER)
        var loggedOutCred = AFOAuthCredential.retrieveCredentialWithIdentifier(LOGGED_OUT_IDENTIFIER)
        
        if(loggedInCred != nil)
        {
            setCredentials(loggedInCred, type: kAFOAuthPasswordCredentialsGrantType)
        } else if(loggedOutCred != nil) {
            setCredentials(loggedOutCred, type: kAFOAuthClientCredentialsGrantType)
        }
        
        httpClient.responseSerializer = AFJSONResponseSerializer(readingOptions: NSJSONReadingOptions.AllowFragments)
        httpClient.requestSerializer = AFJSONRequestSerializer(writingOptions: NSJSONWritingOptions.allZeros)
        
        //TODO fix this because this is really bad
        oauthClient?.securityPolicy.allowInvalidCertificates = true
        httpClient.securityPolicy.allowInvalidCertificates = true
    }
    
    func archiveTo(archivePath: String) {
        if(self.creds != nil) {
            if(!NSKeyedArchiver.archiveRootObject(self.creds!, toFile: archivePath))
            {
                NSLog("failed to archive credentials at " + archivePath)
            }
        }
    }
    
    func isLoggedIn() -> Bool
    {
        if(grantType == kAFOAuthPasswordCredentialsGrantType)
        {
            return true
        } else {
            return false
        }
    }
    //    (void)observeNetworkAvailabilityChanges: (OACSNetStatusHelper *)observer {
    //    [self.httpClient setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
    //    [observer updateStatus:status];
    //    }];
    //    }
    
    func networkAvailable() -> AFNetworkReachabilityStatus
    {
        return reachabilityManager.networkReachabilityStatus
    }
    
    func isAuthorized() -> Bool
    {
        if(self.oauthClient != nil && self.creds != nil)
        {
            return true
        } else {
            return false
        }
    }
    
    func GET(path: NSString, parameters: AnyObject!, retry: Bool, success: (AFHTTPRequestOperation!, AnyObject!) -> Void, failure: (AFHTTPRequestOperation!, NSError!) -> Void) -> Void
    {
        let getClosure = { self.GET(path as String, parameters: parameters, onSuccess: { (op: AFHTTPRequestOperation!, data: AnyObject!) -> Void in
            success(op, data)
            }) { (op: AFHTTPRequestOperation!, error: NSError!) -> Void in
                failure(op, error)
            }
        }
        
        if(self.creds == nil)
        {
            var errors = ["error": "Application is not authorized"]
            var error = NSError(domain: "com.yazda.yazda-ios", code: 1, userInfo: errors)
            
            failure(AFHTTPRequestOperation(), error)
        } else if(self.creds!.expired && retry) {
            self.refreshToken({ () -> Void in
                getClosure()
                }, onFailure: failure)
        } else {
            getClosure()
        }
        
    }
    
    
    func DELETE(URLString: String, parameters: AnyObject!, retry: Bool, success: (AFHTTPRequestOperation!, AnyObject!) -> Void, failure: (AFHTTPRequestOperation!, NSError!) -> Void)
    {
        let deleteClosure = { self.DELETE(URLString, parameters: parameters, onSuccess: { (op, data) -> Void in
            success(op, data)
            }) { (op: AFHTTPRequestOperation!, error: NSError!) -> Void in
                failure(op, error)
            }
        }
        
        if(self.creds == nil)
        {
            var errors = ["error": "Application is not authorized"]
            var error = NSError(domain: "com.yazda.yazda-ios", code: 1, userInfo: errors)
            
            failure(AFHTTPRequestOperation(), error)
        } else if(self.creds!.expired && retry) {
            self.refreshToken({ () -> Void in
                deleteClosure()
                }, onFailure: failure)
        } else {
            deleteClosure()
        }
    }

    
    func POST(URLString: String, parameters: AnyObject!, retry: Bool, success: (AFHTTPRequestOperation!, AnyObject!) -> Void, failure: (AFHTTPRequestOperation!, NSError!) -> Void)
    {
        let postClosure = { self.POST(URLString, parameters: parameters, onSuccess: { (op, data) -> Void in
            success(op, data)
            }) { (op: AFHTTPRequestOperation!, error: NSError!) -> Void in
                failure(op, error)
            }
        }
        
        if(self.creds == nil)
        {
            var errors = ["error": "Application is not authorized"]
            var error = NSError(domain: "com.yazda.yazda-ios", code: 1, userInfo: errors)
            
            failure(AFHTTPRequestOperation(), error)
        } else if(self.creds!.expired && retry) {
            self.refreshToken({ () -> Void in
                postClosure()
                }, onFailure: failure)
        } else {
            postClosure()
        }
    }
    
    func UPLOAD(URLString: String, parameters: AnyObject!, fileName: String!, file: NSData!, retry: Bool, success: (AFHTTPRequestOperation!, AnyObject!) -> Void, failure: (AFHTTPRequestOperation!, NSError!) -> Void)
    {
        let postClosure = { self.UPLOAD(URLString, parameters: parameters, fileName: fileName, file: file, onSuccess: { (op, data) -> Void in
            success(op, data)
            }) { (op: AFHTTPRequestOperation!, error: NSError!) -> Void in
                failure(op, error)
            }
        }
        
        if(self.creds == nil)
        {
            var errors = ["error": "Application is not authorized"]
            var error = NSError(domain: "com.yazda.yazda-ios", code: 1, userInfo: errors)
            
            failure(AFHTTPRequestOperation(), error)
        } else if(self.creds!.expired && retry) {
            self.refreshToken({ () -> Void in
                postClosure()
                }, onFailure: failure)
        } else {
            postClosure()
        }
    }
    
    func PUT(URLString: String, parameters: AnyObject!, retry: Bool, success: (AFHTTPRequestOperation!, AnyObject!) -> Void, failure: (AFHTTPRequestOperation!, NSError!) -> Void)
    {
        let putClosure = { self.PUT(URLString, parameters: parameters, onSuccess: { (op, data) -> Void in
            success(op, data)
            }) { (op: AFHTTPRequestOperation!, error: NSError!) -> Void in
                failure(op, error)
            }
        }
        
        if(self.creds == nil)
        {
            var errors = ["error": "Application is not authorized"]
            var error = NSError(domain: "com.yazda.yazda-ios", code: 1, userInfo: errors)
            
            failure(AFHTTPRequestOperation(), error)
        } else if(self.creds!.expired && retry) {
            self.refreshToken({ () -> Void in
                putClosure()
                }, onFailure: failure)
        } else {
            putClosure()
        }
    }
    
    
    func PATCH(URLString: String, parameters: AnyObject!, retry: Bool, success: (AFHTTPRequestOperation!, AnyObject!) -> Void, failure: (AFHTTPRequestOperation!, NSError!) -> Void)
    {
        let patchClosure = { self.PATCH(URLString, parameters: parameters, onSuccess: { (op, data) -> Void in
            success(op, data)
            }) { (op: AFHTTPRequestOperation!, error: NSError!) -> Void in
                failure(op, error)
            }
        }
        
        if(self.creds == nil)
        {
            var errors = ["error": "Application is not authorized"]
            var error = NSError(domain: "com.yazda.yazda-ios", code: 1, userInfo: errors)
            
            failure(AFHTTPRequestOperation(), error)
        } else if(self.creds!.expired && retry) {
            self.refreshToken({ () -> Void in
                patchClosure()
                }, onFailure: failure)
        } else {
            patchClosure()
        }
    }
    
    func authorizeUser(username: String, password: String, onSuccess: () ->(Void), onFailure: (failure: NSString) ->())
    {
        if(self.creds == nil || (self.grantType == kAFOAuthClientCredentialsGrantType))
        {
            self.oauthClient!.authenticateUsingOAuthWithURLString(self.tokenPath, username: username, password: password, scope: "", success: { (credential: AFOAuthCredential!) -> Void in
                
                AFOAuthCredential.storeCredential(credential, withIdentifier: self.LOGGED_IN_IDENTIFIER)
                self.setCredentials(credential, type: kAFOAuthPasswordCredentialsGrantType)
                
                client!.account.me({ (user) -> Void in
                    let u = user as! NSDictionary
                    let me = user.objectForKey("user") as! NSDictionary
                    let id = me.objectForKey("id") as! Int
                    var defaults = NSUserDefaults.standardUserDefaults()
                    
                    defaults.setInteger(id, forKey: "currentUserId")
                    onSuccess()
                    
                    }, failure: { (op: AFHTTPRequestOperation, error: NSError) -> Void in
                        var uiInfo:NSDictionary! = error.userInfo!
                        
                        onFailure(failure: uiInfo.valueForKey(NSLocalizedDescriptionKey) as! String)
                })
                
                }) { (error: NSError!) -> Void in
                    NSLog("OAuth client authorization error: %@", error)
                    
                    var uiInfo:NSDictionary! = error.userInfo!
                    self.resignAuthorization()
                    
                    var errorMsg = uiInfo.valueForKey(NSLocalizedDescriptionKey) as? String
                    
                    if(errorMsg == nil) {
                        errorMsg = NSString(format: "Something went wrong and we're not sure.  Send this to us! %@", uiInfo) as String
                    }
                    onFailure(failure: errorMsg!)
            }
        }
    }
    
    
    func authorizeClientCredentials(onSuccess: () -> Void, onFailure: (failure: NSString) ->())
    {
        if(self.creds == nil)
        {
            self.oauthClient!.authenticateUsingOAuthWithURLString(self.tokenPath, scope: "", success: { (credential: AFOAuthCredential!) -> Void in
                
                AFOAuthCredential.storeCredential(credential, withIdentifier: self.LOGGED_OUT_IDENTIFIER)
                self.setCredentials(credential, type: kAFOAuthClientCredentialsGrantType)
                onSuccess()
                
                }) { (error: NSError!) -> Void in
                    NSLog("OAuth client authorization error: %@", error)
                    
                    var uiInfo:NSDictionary! = error.userInfo!
                    self.resignAuthorization()
                    
                    var errorMsg = uiInfo.valueForKey(NSLocalizedDescriptionKey) as? String
                    
                    if(errorMsg == nil) {
                        errorMsg = NSString(format: "Something went wrong and we're not sure.  Send this to us! %@", uiInfo) as String
                    }
                    onFailure(failure: errorMsg!)
            }
        }
    }
    
    func resignAuthorization()
    {
        //        self.oauthClient.clearAuthorizationHeader() TODO FIX THIS YO
        
        //TODO only navigate if logged in
        if(self.isLoggedIn()) {
            Common.navigateToMainStoryboard()
        }
        
        AFOAuthCredential.deleteCredentialWithIdentifier(self.LOGGED_IN_IDENTIFIER)
        AFOAuthCredential.deleteCredentialWithIdentifier(self.LOGGED_OUT_IDENTIFIER)
        self.creds = nil
    }
    
    private func prepareForRequest()
    {
        if(object_getClassName(httpClient.responseSerializer) != object_getClassName(AFJSONResponseSerializer))
        {
            httpClient.responseSerializer = AFJSONResponseSerializer(readingOptions: NSJSONReadingOptions.AllowFragments)
        }
        
        if(object_getClassName(httpClient.requestSerializer) != object_getClassName(AFJSONRequestSerializer))
        {
            httpClient.requestSerializer = AFJSONRequestSerializer(writingOptions: NSJSONWritingOptions.allZeros)
        }
        
        httpClient.requestSerializer.setValue("Bearer \(self.creds!.accessToken)", forHTTPHeaderField: "Authorization")
    }
    
    private func prepareForUploadRequest()
    {
        if(object_getClassName(httpClient.responseSerializer) != object_getClassName(AFJSONResponseSerializer))
        {
            httpClient.responseSerializer = AFJSONResponseSerializer(readingOptions: NSJSONReadingOptions.AllowFragments)
        }
        
        if(object_getClassName(httpClient.requestSerializer) != object_getClassName(AFHTTPRequestSerializer))
        {
            httpClient.requestSerializer = AFHTTPRequestSerializer()
        }
        
        httpClient.requestSerializer.setValue("Bearer \(self.creds!.accessToken)", forHTTPHeaderField: "Authorization")
    }
    
    private func refreshToken(onSuccess: () ->Void, onFailure: (AFHTTPRequestOperation!, NSError!)-> Void)
    {
        if(self.creds!.refreshToken != nil && self.grantType == kAFOAuthClientCredentialsGrantType)
        {
            self.oauthClient!.authenticateUsingOAuthWithURLString(self.tokenPath, refreshToken: self.creds!.refreshToken, success: { (credential: AFOAuthCredential!) -> Void in
                
                
                AFOAuthCredential.storeCredential(credential, withIdentifier: self.LOGGED_IN_IDENTIFIER)
                self.setCredentials(credential, type: kAFOAuthClientCredentialsGrantType)
                onSuccess()
                
                }, failure: { (error: NSError!) -> Void in
                    var errors = ["error": "Failed to authorize"]
                    var error = NSError(domain: "com.yazda.yazda-ios", code: 1, userInfo: errors)
                    
                    self.resignAuthorization()
                    onFailure(AFHTTPRequestOperation(), error)
            })
        } else if(self.creds!.refreshToken != nil && self.grantType == kAFOAuthPasswordCredentialsGrantType) {
            
            self.oauthClient!.authenticateUsingOAuthWithURLString(self.tokenPath, refreshToken: self.creds!.refreshToken, success: { (credential: AFOAuthCredential!) -> Void in
                
                
                AFOAuthCredential.storeCredential(credential, withIdentifier: self.LOGGED_IN_IDENTIFIER)
                self.setCredentials(credential, type: kAFOAuthPasswordCredentialsGrantType)
                onSuccess()
                
                }, failure: { (error: NSError!) -> Void in
                    var errors = ["error": "Failed to authorize"]
                    var error = NSError(domain: "com.yazda.yazda-ios", code: 1, userInfo: errors)
                    
                    self.resignAuthorization()
                    
                    onFailure(AFHTTPRequestOperation(), error)
            })
        } else {
            var errors = ["error": "Application is not authorized"]
            var error = NSError(domain: "com.yazda.yazda-ios", code: 1, userInfo: errors)
            self.resignAuthorization()
            
            onFailure(AFHTTPRequestOperation(), error)
        }
    }
    
    private func GET(URLString: String, parameters: AnyObject!, onSuccess success: (AFHTTPRequestOperation!, AnyObject!) -> Void, failure: (AFHTTPRequestOperation!, NSError!) -> Void)
    {
        prepareForRequest()
        
        self.httpClient.GET(URLString, parameters: parameters, success: { (op: AFHTTPRequestOperation!, data: AnyObject!) -> Void in
            success(op, data)
            }) { (op: AFHTTPRequestOperation!, error: NSError!) -> Void in
                failure(op, error)
        }
    }
    
    private func POST(URLString: String, parameters: AnyObject!, onSuccess success: (AFHTTPRequestOperation!, AnyObject!) -> Void, failure: (AFHTTPRequestOperation!, NSError!) -> Void)
    {
        prepareForRequest()
        
        
        self.httpClient.POST(URLString, parameters: parameters, success: { (op, data) -> Void in
            
            success(op, data)
            
            }) { (op: AFHTTPRequestOperation!, error: NSError!) -> Void in
                failure(op, error)
        }
        
    }
    
    private func PATCH(URLString: String, parameters: AnyObject!, onSuccess success: (AFHTTPRequestOperation!, AnyObject!) -> Void, failure: (AFHTTPRequestOperation!, NSError!) -> Void)
    {
        prepareForRequest()
        
        self.httpClient.PATCH(URLString, parameters: parameters, success: { (op, data) -> Void in
            success(op, data)
            
            }) { (op: AFHTTPRequestOperation!, error: NSError!) -> Void in
                failure(op, error)
        }
    }
    
    private func PUT(URLString: String, parameters: AnyObject!, onSuccess success: (AFHTTPRequestOperation!, AnyObject!) -> Void, failure: (AFHTTPRequestOperation!, NSError!) -> Void)
    {
        prepareForRequest()
        
        self.httpClient.PUT(URLString, parameters: parameters, success: { (op, data) -> Void in
            success(op, data)
            
            }) { (op: AFHTTPRequestOperation!, error: NSError!) -> Void in
                failure(op, error)
        }
    }
    
    private func DELETE(URLString: String, parameters: AnyObject!, onSuccess success: (AFHTTPRequestOperation!, AnyObject!) -> Void, failure: (AFHTTPRequestOperation!, NSError!) -> Void)
    {
        prepareForRequest()
        
        self.httpClient.DELETE(URLString, parameters: parameters, success: { (op, data) -> Void in
            success(op, data)
            
            }) { (op: AFHTTPRequestOperation!, error: NSError!) -> Void in
                failure(op, error)
        }
    }
    
    private func UPLOAD(URLString: String, parameters: AnyObject!, fileName: String!, file: NSData!, onSuccess success: (AFHTTPRequestOperation!, AnyObject!) -> Void, failure: (AFHTTPRequestOperation!, NSError!) -> Void)
    {
        prepareForUploadRequest()
        
        self.httpClient.POST(URLString, parameters: parameters, constructingBodyWithBlock: { (formData: AFMultipartFormData!) -> Void in
            formData.appendPartWithFormData(file, name: fileName)
            }, success: { (op, data) -> Void in
                success(op, data)
                
            }) { (op: AFHTTPRequestOperation!, error: NSError!) -> Void in
                failure(op, error)
        }
        
    }
    
    private func setCredentials(credential: AFOAuthCredential, type: NSString!)
    {
        self.creds = credential
        self.grantType = type
    }
    //    (void)initConfigurationFrom: (NSDictionary *)config {
    //    self.auth_path = [config objectForKey:@"auth_path"];
    //    self.token_path = [config objectForKey:@"token_path"];
    //    NSString *callback_str = [config objectForKey:@"callback_url"];
    //    self.callback_url = callback_str ? [NSURL URLWithString:callback_str] : nil;
    //    NSString * base_str = [config objectForKey:@"base_url"];
    //    self.base_url = base_str ? [NSURL URLWithString:base_str] : nil;
    //    self.client_key = [config objectForKey:@"client_key"];
    //    self.client_secret = [config objectForKey:@"client_secret"];
    //    }
    
    //    - (NSDictionary *)readDictionaryFromConfig: (NSString *)configPath
    //    {
    //    NSString *errorDesc = nil;
    //    NSData *configXML = [[NSFileManager defaultManager] contentsAtPath:configPath];
    //    NSDictionary *config = (NSDictionary *)[NSPropertyListSerialization
    //    propertyListFromData:configXML
    //    mutabilityOption:NSPropertyListMutableContainersAndLeaves
    //    format:NULL
    //    errorDescription:&errorDesc];
    //    if (!config) {
    //    NSLog(@"OACSAuthClient error reading config %@ is '%@'", configPath, errorDesc);
    //    }
    //    return config;
    //    }
}