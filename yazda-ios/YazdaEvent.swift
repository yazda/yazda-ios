//
//  YazdaEvent.swift
//  yazda-ios
//
//  Created by James Stoup on 3/13/15.
//  Copyright (c) 2015 yazda. All rights reserved.
//

import Foundation
import AFNetworking

class YazdaEvent: YazdaBaseService {
    let BASE_URL = "/adventures"
    
    func index(attending: String?, status: String?, adventureType: String?, lat: Double?, lon: Double?, page: Int?, pageSize: Int?, success: (AnyObject!)->Void, failure: (AFHTTPRequestOperation, NSError)->Void)
    {
        var params = NSMutableDictionary(dictionary: [String : AnyObject]())
        
        addParamUnlessNil(&params, param: attending, forKey: "attending")
        addParamUnlessNil(&params, param: status, forKey: "status")
        addParamUnlessNil(&params, param: adventureType, forKey: "adventure_type")
        addParamUnlessNil(&params, param: lat, forKey: "lat")
        addParamUnlessNil(&params, param: lon, forKey: "lon")
        addParamUnlessNil(&params, param: page, forKey: "page")
        addParamUnlessNil(&params, param: pageSize, forKey: "page_size")
        
        var url = buildURL(BASE_URL, params: "")
        
        YazdaAPI.Contstants.client.GET(url, parameters: params, retry: true, success: { (op: AFHTTPRequestOperation!, object: AnyObject!) -> Void in
            
            NSLog("Getting Adventures with params: %@", params)
            
            success(object)
            
            }) { (op: AFHTTPRequestOperation!, error: NSError!) -> Void in
                
                NSLog("Failed getting adventures with params: %@", params)
                
                failure(op, error)
        }
    }
    
    func show(id: Int!, success: (AnyObject!)->Void, failure: (AFHTTPRequestOperation, NSError)->Void)
    {
        var url = buildURL(BASE_URL, params: String(id))
        
        YazdaAPI.Contstants.client.GET(url, parameters: nil, retry: true, success: { (op: AFHTTPRequestOperation!, data: AnyObject!) -> Void in
            
            NSLog("Getting Adventure with id: %0d", id)
            success(data)
            
            }) { (op: AFHTTPRequestOperation!, error: NSError!) -> Void in
                
                NSLog("Failed getting adventure with id: %0d", id)
                failure(op, error)
        }
    }
    
    func create(adventureType: String!, name: String!, description: String, startTime: NSDate!, endTime: NSDate!, address: String, lat: Double?, lon: Double?, userIds: NSMutableSet!, isPrivate: Bool!, reservationLimit: Int?, success: (AnyObject!)->Void, failure: (AFHTTPRequestOperation, NSError)->Void)
    {
        var url = buildURL(BASE_URL, params: "")
        var params = NSMutableDictionary(dictionary: [String : AnyObject]())
        var start = NSDateFormatter.localizedStringFromDate(startTime, dateStyle: NSDateFormatterStyle.FullStyle, timeStyle: NSDateFormatterStyle.FullStyle)
        var end = NSDateFormatter.localizedStringFromDate(endTime, dateStyle: NSDateFormatterStyle.FullStyle, timeStyle: NSDateFormatterStyle.FullStyle)
        
        addParamUnlessNil(&params, param: adventureType, forKey: "adventure_type")
        addParamUnlessNil(&params, param: name, forKey: "name")
        addParamUnlessNil(&params, param: description, forKey: "description")
        addParamUnlessNil(&params, param: start, forKey: "start_time")
        addParamUnlessNil(&params, param: end, forKey: "end_time")
        addParamUnlessNil(&params, param: address, forKey: "address")
        addParamUnlessNil(&params, param: lat, forKey: "lat")
        addParamUnlessNil(&params, param: lon, forKey: "lon")
        addParamUnlessNil(&params, param: userIds.allObjects, forKey: "user_ids")
        addParamUnlessNil(&params, param: isPrivate, forKey: "private")
        addParamUnlessNil(&params, param: reservationLimit, forKey: "reservation_limit")
        
        YazdaAPI.Contstants.client.POST(url, parameters: params, retry: true, success: { (op: AFHTTPRequestOperation!, data: AnyObject!) -> Void in
            
            NSLog("Creating Adventure with parameters: %@", params)
            success(data)
            
            }) { (op: AFHTTPRequestOperation!, error: NSError!) -> Void in
                
                NSLog("Failed creating Adventure with id: %@", params)
                failure(op, error)
        }
        
    }
    
    func update(id: Int!, adventureType: String!, name: String!, description: String, startTime: NSDate!, endTime: NSDate!, address: String, lat: Float!, lon: Float!, userIds: [Int]!, isPrivate: Bool, success: (AnyObject!)->Void, failure: (AFHTTPRequestOperation, NSError)->Void)
    {
        var url = buildURL(BASE_URL, params: String(id))
        var params = NSMutableDictionary(dictionary: [String : AnyObject]())
        
        addParamUnlessNil(&params, param: adventureType, forKey: "adventure_type")
        addParamUnlessNil(&params, param: name, forKey: "name")
        addParamUnlessNil(&params, param: description, forKey: "description")
        addParamUnlessNil(&params, param: startTime, forKey: "start_time")
        addParamUnlessNil(&params, param: endTime, forKey: "end_time")
        addParamUnlessNil(&params, param: address, forKey: "address")
        addParamUnlessNil(&params, param: lat, forKey: "lat")
        addParamUnlessNil(&params, param: lon, forKey: "lon")
        addParamUnlessNil(&params, param: userIds, forKey: "user_ids")
        addParamUnlessNil(&params, param: isPrivate, forKey: "private")
        
        YazdaAPI.Contstants.client.PUT(url, parameters: params, retry: true, success: { (op: AFHTTPRequestOperation!, data: AnyObject!) -> Void in
            
            NSLog("Updating Adventure with parameters: %@", params)
            success(data)
            
            }) { (op: AFHTTPRequestOperation!, error: NSError!) -> Void in
                
                NSLog("Failed updating Adventure with id: %@", params)
                failure(op, error)
        }
        
    }
    
    
    func cancel(id: Int!, success: (AnyObject!)->Void, failure: (AFHTTPRequestOperation, NSError)->Void)
    {
        var url = buildURL(BASE_URL, params: String(id))

        YazdaAPI.Contstants.client.DELETE(url, parameters: nil, retry: true, success: { (op: AFHTTPRequestOperation!, data: AnyObject!) -> Void in
            
            NSLog("Canceling Adventure with id: %0d", id)
            success(data)
            
            }) { (op: AFHTTPRequestOperation!, error: NSError!) -> Void in
                
                NSLog("Failed canceling Adventure with id: %0d", id)
                failure(op, error)
        }
        
    }
}
