//
//  EventsTableViewController.swift
//  yazda-ios
//
//  Created by James Stoup on 3/15/15.
//  Copyright (c) 2015 yazda. All rights reserved.
//

import UIKit
import CoreLocation
import AFNetworking

class EventsTableViewController: BaseTableViewController {
    let cellIdentifier = "eventCell"
    
    var locationManager:CLLocationManager?
    var currentPage = 1
    var totalPages = 0
    var selectedEvent:NSDictionary?
    var sections:NSMutableDictionary?
    var sortedDays:NSArray?
    var lat:Double?
    var lon:Double?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        self.locationManager = CLLocationManager()
        self.locationManager!.delegate = self
        self.locationManager!.desiredAccuracy = kCLLocationAccuracyBest
        
        if CLLocationManager.authorizationStatus() == .NotDetermined
        {
            self.locationManager!.requestWhenInUseAuthorization()
        }
        
        self.sections = NSMutableDictionary()
        self.sortedDays = NSArray()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        self.locationManager!.startUpdatingLocation()
        self.refresh()  //TODO we should only refresh when an event is created or after a duration of time
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return self.sections!.count
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        var dateRepresentingThisDay = self.sortedDays?.objectAtIndex(section) as! NSDate
        
        return Common.toDateString(dateRepresentingThisDay)
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        var dateRepresentingThisDay = self.sortedDays?.objectAtIndex(section) as! NSDate
        var eventsOnThisDay = self.sections?.objectForKey(dateRepresentingThisDay) as! NSArray
        
        if(currentPage < totalPages && self.sortedDays?.count > section)
        {
            return eventsOnThisDay.count + 1
        }
        
        return eventsOnThisDay.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if(indexPath.section < self.sortedDays?.count)
        {
            return self.eventCellForIndexPath(indexPath)
        } else {
            return self.loadingCell()
        }
    }
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if(cell.tag == kAnnotationType)
        {
            self.fetchEvents()
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        let dateRepresentingThisDay = self.sortedDays?.objectAtIndex(indexPath.section) as? NSDate
        
        if(dateRepresentingThisDay != nil) {
            let eventsOnThisDay = self.sections?.objectForKey(dateRepresentingThisDay!) as? NSArray
            
            if(eventsOnThisDay != nil) {
                self.selectedEvent = eventsOnThisDay![indexPath.row] as? NSDictionary
                self.performSegueWithIdentifier("showEvent", sender: self)
            }
        }
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 180
    }
    
    func refresh()
    {
        self.sections = NSMutableDictionary()
        self.sortedDays = NSArray()
        self.currentPage = 1
        self.totalPages = 0
        self.fetchEvents()
    }
    
    private func eventCellForIndexPath(indexPath: NSIndexPath) -> UITableViewCell
    {
        var cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as? EventTableViewCell
        
        if(cell == nil)
        {
            cell = EventTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdentifier)
        }
        
        let dateRepresentingThisDay = self.sortedDays?.objectAtIndex(indexPath.section) as! NSDate
        let eventsOnThisDay = self.sections?.objectForKey(dateRepresentingThisDay) as! NSArray
        let event = eventsOnThisDay.objectAtIndex(indexPath.row) as! NSDictionary
        
        cell!.setEventName(event.objectForKey("name") as! String)
        cell!.setDateLabel(event.objectForKey("start_time") as! String, endDate: event.objectForKey("end_time") as! String)
        cell!.setAdventureTypeLabel(event.objectForKey("adventure_type") as! String)
        cell!.setInvites(event.objectForKey("attendings") as! NSArray)
        cell!.addFriend(event.objectForKey("owner") as! NSDictionary)
        
        return cell!
    }
    
    private func loadingCell() -> UITableViewCell
    {
        var cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: nil)
        var activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.WhiteLarge)
        
        activityIndicator.center = cell.center
        cell.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        cell.tag = kAnnotationType
        
        return cell
    }
    
    private func fetchEvents()
    {
        self.disableActions()
        
        //TODO narrow this down better
        client!.adventures.index(nil, status: nil, adventureType: nil, lat: self.lat, lon: self.lon, page: currentPage, pageSize: nil, success: { (data: AnyObject!) -> Void in
            let objs = data as! NSDictionary!
            let now = NSDate.dateAtBeginningOfDayForDate(NSDate())
            let calendar = NSCalendar.currentCalendar()
            
            self.totalPages = objs.objectForKey("page_count") as! Int
            
            for obj in objs.objectForKey("adventures") as! NSArray
            {
                let event = obj as! NSDictionary
                let date = Common.toDate(event.objectForKey("start_time") as! String)
                let dif = calendar.compareDate(now, toDate: date, toUnitGranularity: NSCalendarUnit.CalendarUnitDay)
                
                var dateRepresentingThisDay:NSDate
                
                if(dif == NSComparisonResult.OrderedDescending || dif == NSComparisonResult.OrderedSame)
                {
                    dateRepresentingThisDay = NSDate.dateAtBeginningOfDayForDate(now)
                } else {
                    dateRepresentingThisDay = NSDate.dateAtBeginningOfDayForDate(date)
                }
                
                var eventsOnThisDay = self.sections!.objectForKey(dateRepresentingThisDay) as? NSMutableArray
                println(dateRepresentingThisDay)
                
                if(eventsOnThisDay == nil)
                {
                    eventsOnThisDay = NSMutableArray()
                    
                    self.sections!.setObject(eventsOnThisDay!, forKey:dateRepresentingThisDay)
                }
                
                if(!eventsOnThisDay!.containsObject(event))
                {
                    eventsOnThisDay!.addObject(event)
                }
            }
            
            let keys = self.sections!.allKeys as NSArray
            
            self.sortedDays = keys.sortedArrayUsingSelector(Selector("compare:"))
            
            self.currentPage++
            self.tableView.reloadData()
            
            self.enableAction()
            
            }) { (op: AFHTTPRequestOperation, error: NSError) -> Void in
                //TODO error
                
                self.enableAction()
        }
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        
        if(segue.identifier == "showEvent")
        {
            let controller = segue.destinationViewController as! EventViewController
            
            controller.event = self.selectedEvent
        }
    }
}

extension EventsTableViewController: CLLocationManagerDelegate {
    func locationManager(manager: CLLocationManager!, didUpdateToLocation newLocation: CLLocation!, fromLocation oldLocation: CLLocation!) {
        
        var currentLocation = newLocation
        NSLog("didUpdateToLocation: %@", newLocation)
        
        if(currentLocation != nil)
        {
            self.lat = currentLocation.coordinate.latitude
            self.lon = currentLocation.coordinate.longitude
            locationManager?.stopUpdatingLocation()
        }
    }
    
    func locationManager(manager: CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if(status == CLAuthorizationStatus.AuthorizedWhenInUse)
        {
            manager.startUpdatingLocation()
        }
    }
}