//
//  YazdaBaseService.swift
//  yazda-ios
//
//  Created by James Stoup on 3/13/15.
//  Copyright (c) 2015 yazda. All rights reserved.
//

import Foundation
import AFNetworking

class YazdaBaseService: NSObject {
    
    internal func buildURL(base_url: String!, params: String...) -> String
    {
        var url:String = "/\(YazdaAPI.Contstants.VERSION)/\(base_url)"
        
        for path in params
        {
            if(count(path) > 0 && path[path.startIndex] == "/")
            {
                url += path
            } else {
                url += "/\(path)"
            }
        }
        
        return url
    }
    
    internal func addParamUnlessNil(inout parameters: NSMutableDictionary, param: AnyObject?, forKey: String!)
    {
        if(param != nil)
        {
            parameters.setValue(param!, forKey: forKey)
        }
    }
}