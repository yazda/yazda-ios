//
//  CreateEventViewController.swift
//  yazda-ios
//
//  Created by James Stoup on 3/16/15.
//  Copyright (c) 2015 yazda. All rights reserved.
//

import UIKit
import MapKit
import AFNetworking
import FontAwesome
import FontAwesomeIconFactory

class CreateEventViewController: BaseViewController, UIPopoverPresentationControllerDelegate, MKMapViewDelegate {
    
    var types = [("mountain_biking", "Mountain Biking"), ("hiking", "Hiking")]
    var userIds:NSMutableSet?
    var location:CLLocationCoordinate2D?
    var address:String = ""
    var eventTypeValue:String = ""
    var startTimeValue:NSDate = NSDate()
    var endTimeValue:NSDate = NSDate()
    
    @IBOutlet weak var eventName: YazdaTextField!
    @IBOutlet weak var eventDescription: UITextView!
    @IBOutlet weak var eventType: YazdaTextField!
    @IBOutlet weak var startTime: YazdaTextField!
    @IBOutlet weak var endTime: YazdaTextField!
    @IBOutlet weak var isPublic: UISwitch!
    @IBOutlet weak var locationText: UILabel!
    @IBOutlet weak var maxInvites: UITextField!
    @IBOutlet weak var descriptionText: YazdaTextView!
    @IBOutlet weak var locationButton: YazdaButton!
    
    @IBOutlet var eventTypePicker: UIPickerView!
    @IBOutlet var startDatePicker: UIDatePicker!
    @IBOutlet var endDatePicker: UIDatePicker!
    
    var friendCollectionView: FriendCollectionViewDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.userIds = NSMutableSet()
        
        self.eventName.delegate = self
        self.eventType.delegate = self
        self.startTime.delegate = self
        self.endTime.delegate = self
        self.descriptionText.delegate = self
        
        self.maxInvites.placeholder = "\u{221E}"
        
        var factory = NIKFontAwesomeIconFactory.buttonIconFactory()
        factory.size = 25
        self.locationButton.setTitle("Set Event Location", forState: UIControlState.Normal)
        self.locationButton.setImage(factory.createImageForIcon(NIKFontAwesomeIcon.MapMarker), forState: UIControlState.Normal)
        
        self.eventName.addIcon(NIKFontAwesomeIcon.Edit)
        self.eventType.addIcon(NIKFontAwesomeIcon.Question)
        self.startTime.addIcon(NIKFontAwesomeIcon.CalendarO)
        self.endTime.addIcon(NIKFontAwesomeIcon.CalendarO)
        
        var eventTypeToolbar = UIToolbar(frame: CGRectMake(0, 0, 320, 44))
        var eventTypeDone = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Done, target: self, action: "chooseEventType")
        eventTypeToolbar.items = [eventTypeDone]
        
        self.eventTypePicker = UIPickerView()
        self.eventTypePicker.delegate = self
        self.eventTypePicker.dataSource = self
        self.eventType.inputView = self.eventTypePicker
        self.eventType.inputAccessoryView = eventTypeToolbar
        
        var startTimeToolbar = UIToolbar(frame: CGRectMake(0, 0, 320, 44))
        var startTimeDone = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Done, target: self, action: "closeStartDate")
        startTimeToolbar.items = [startTimeDone]
        
        self.startDatePicker = UIDatePicker()
        self.startDatePicker.datePickerMode = UIDatePickerMode.DateAndTime
        self.startDatePicker.minuteInterval = 15
        self.startDatePicker.alpha = 0.8
        self.startDatePicker.opaque = false
        self.startDatePicker.minimumDate = NSDate()
        self.startTime.inputView = self.startDatePicker
        self.startTime.inputAccessoryView = startTimeToolbar
        self.startDatePicker.addTarget(self, action: "pickStartDate:", forControlEvents: UIControlEvents.ValueChanged)
        
        var endTimeToolbar = UIToolbar(frame: CGRectMake(0, 0, 320, 44))
        var endTimeDone = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Done, target: self, action: "closeEndDate")
        endTimeToolbar.items = [endTimeDone]
        
        self.endDatePicker = UIDatePicker()
        self.endDatePicker.datePickerMode = UIDatePickerMode.DateAndTime
        self.endDatePicker.minuteInterval = 15
        self.endDatePicker.alpha = 0.8
        self.endDatePicker.opaque = false
        self.endDatePicker.minimumDate = NSDate()
        self.endTime.inputView = self.endDatePicker
        self.endTime.inputAccessoryView = endTimeToolbar
        self.endDatePicker.addTarget(self, action: "pickEndDate:", forControlEvents: UIControlEvents.ValueChanged)
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Create", style: UIBarButtonItemStyle.Done, target: self, action: "create")
    }
    
    func create()
    {
        self.disableActions()
        
        client!.adventures.create(self.eventTypeValue, name: self.eventName.text, description: self.eventDescription.text, startTime: self.startTimeValue, endTime: self.endTimeValue, address: self.address, lat: self.location?.latitude, lon: self.location?.longitude, userIds: userIds, isPrivate: !(isPublic.on), reservationLimit: self.maxInvites.text.toInt(), success: { (data) -> Void in
            
            self.enableActions()
            self.navigationController?.popViewControllerAnimated(true)
            
            }) { (op: AFHTTPRequestOperation, error: NSError) -> Void in
                
                if let obj = op.responseObject as? NSDictionary {
                    let messages = obj.objectForKey("errors") as! [String]
                    
                    YazdaAlert(parentView: self, messages: messages, withDelay: Common.Interval)
                }
                self.enableActions()
        }
    }
    
    func setEventPickerValue(row: Int)
    {
        self.eventTypeValue = self.types[row].0
        self.eventType.text = self.types[row].1
    }
    
    func chooseEventType()
    {
        let row = self.eventTypePicker.selectedRowInComponent(0)
        
        self.setEventPickerValue(row)
        self.eventType.resignFirstResponder()
    }
    
    func closeStartDate()
    {
        self.pickStartDate(self.startDatePicker)
        self.startTime.resignFirstResponder()
    }
    
    func closeEndDate()
    {
        self.pickEndDate(self.endDatePicker)
        self.endTime.resignFirstResponder()
    }
    
    func pickStartDate(sender: UIDatePicker)
    {
        self.startTimeValue = sender.date
        self.startTime.text = Common.formatDateAndTime(sender.date)
    }
    
    func pickEndDate(sender: UIDatePicker)
    {
        self.endTimeValue = sender.date
        self.endTime.text = Common.formatDateAndTime(sender.date)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        // Return no adaptive presentation style, use default presentation behaviour
        return .None
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "inviteFriends"
        {
            if let controller = segue.destinationViewController as? InviteFriendsTableViewController
            {
                controller.isInviteFriends = true
                controller.userIds = self.userIds
                controller.delegate = self
            }
        } else if(segue.identifier == "pickLocation") {
            if let controller = segue.destinationViewController as? SetLocationViewController
            {
                controller.delegate = self
            }
        } else if(segue.identifier == "inviteFriendsCollection") {
            self.friendCollectionView = segue.destinationViewController as! FriendCollectionViewController
        }
    }
}

extension CreateEventViewController: FriendTableViewDelegate {
    func friendTableViewControllerAddFriend(controller: UIViewController, user: NSDictionary!)
    {
        let id = user.objectForKey("id") as! Int
        
        userIds?.addObject(id)
        self.friendCollectionView!.friendCollectionViewDelegateAddFriend(user)
    }
    
    func friendTableViewControllerRemoveFriend(controller: UIViewController, user: NSDictionary!)
    {
        let id = user.objectForKey("id") as! Int
        userIds?.removeObject(id)
        self.friendCollectionView!.friendCollectionViewDelegateRemoveFriend(user)
    }
}

extension CreateEventViewController: SetLocationViewDelegate {
    func setLocationViewDelegateSelectLoction(location: CLLocationCoordinate2D!, address: String!) {
        self.location = location
        self.address = address
        self.locationText.text = address
    }
}

extension CreateEventViewController: UIPickerViewDataSource {
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 2
    }
}

extension CreateEventViewController: UIPickerViewDelegate {
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        return self.types[row].1
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.setEventPickerValue(row)
    }
}
