//
//  MoreTableViewController.swift
//  yazda-ios
//
//  Created by James Stoup on 6/2/15.
//  Copyright (c) 2015 yazda. All rights reserved.
//

import UIKit
import FontAwesomeIconFactory

class MoreTableViewController: UITableViewController {
    @IBOutlet weak var twitterBtn: UIButton!
    @IBOutlet weak var facebookBtn: UIButton!
    @IBOutlet weak var youtubeBtn: UIButton!
    
    @IBAction func openTwitter(sender: AnyObject) {
        if !UIApplication.sharedApplication().openURL(NSURL(string: "twitter://user?screen_name=yazda_app")!) {
            UIApplication.sharedApplication().openURL(NSURL(string: "https://twitter.com/yazda_app")!)
        }
    }
    
    @IBAction func openFacebook(sender: AnyObject) {
        if !UIApplication.sharedApplication().openURL(NSURL(string: "fb://profile/1532609843695322")!) {
            UIApplication.sharedApplication().openURL(NSURL(string: "https://www.facebook.com/pages/Yazda/1532609843695322")!)
        }
    }
    
    @IBAction func openYoutube(sender: AnyObject) {
        if !UIApplication.sharedApplication().openURL(NSURL(string: "youtube://user/UCcuKd0jf72qlkiC_Jg-Z2wQ")!) {
            UIApplication.sharedApplication().openURL(NSURL(string: "https://www.youtube.com/user/UCcuKd0jf72qlkiC_Jg-Z2wQ")!)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        
        var factory = NIKFontAwesomeIconFactory.buttonIconFactory()
        factory.size = 25
        
        self.twitterBtn.setImage(factory.createImageForIcon(NIKFontAwesomeIcon.TwitterSquare), forState: UIControlState.Normal)
        self.facebookBtn.setImage(factory.createImageForIcon(NIKFontAwesomeIcon.FacebookSquare), forState: UIControlState.Normal)
        
        factory.colors = [UIColor.redColor()]
        self.youtubeBtn.setImage(factory.createImageForIcon(NIKFontAwesomeIcon.YoutubeSquare), forState: UIControlState.Normal)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) as! UITableViewCell
    
    // Configure the cell...
    
    return cell
    }
    */
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return NO if you do not want the specified item to be editable.
    return true
    }
    */
    
    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    if editingStyle == .Delete {
    // Delete the row from the data source
    tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
    } else if editingStyle == .Insert {
    // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
    }
    */
    
    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
    
    }
    */
    
    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return NO if you do not want the item to be re-orderable.
    return true
    }
    */
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    }
    */
    
}
