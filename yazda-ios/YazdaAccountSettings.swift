//
//  YazdaAccountSettings.swift
//  yazda-ios
//
//  Created by James Stoup on 3/15/15.
//  Copyright (c) 2015 yazda. All rights reserved.
//

import Foundation
import AFNetworking

class YazdaAccountSettings: YazdaBaseService {
    let BASE_URL = "/account"
    
    func settings(success: (AnyObject!)->Void, failure: (AFHTTPRequestOperation, NSError)->Void)
    {
        var url = buildURL(BASE_URL, params: "settings")
        
        YazdaAPI.Contstants.client.GET(url, parameters: nil, retry: true, success: { (op: AFHTTPRequestOperation!, object: AnyObject!) -> Void in
            
            NSLog("Getting account settings")
            
            success(object)
            
            }) { (op: AFHTTPRequestOperation!, error: NSError!) -> Void in
                
                NSLog("Failed getting account settings")
                
                failure(op, error)
        }
    }
    
    func me(success: (AnyObject!)->Void, failure: (AFHTTPRequestOperation, NSError)->Void) {
        var url = buildURL(BASE_URL, params: "me")
        
        YazdaAPI.Contstants.client.GET(url, parameters: nil, retry: true, success: { (op: AFHTTPRequestOperation!, object: AnyObject!) -> Void in
            
            NSLog("Getting account me")
            
            success(object)
            
            }) { (op: AFHTTPRequestOperation!, error: NSError!) -> Void in
                
                NSLog("Failed getting account me")
                
                failure(op, error)
        }
    }
    
    func update(email: String?, phone: String?, newsletter: Bool?, name: String?, location: String?, description: String?, notifiesInvite: Bool?, notifiesFriendRequest: Bool?, viewsMountainBiking: Bool?, viewsHiking: Bool?, success: (AnyObject!)->Void, failure: (AFHTTPRequestOperation, NSError)->Void)
    {
        var url = buildURL(BASE_URL, params: "settings")
        var params = NSMutableDictionary(dictionary: [String : AnyObject]())
        
        addParamUnlessNil(&params, param: email, forKey: "email")
        addParamUnlessNil(&params, param: phone, forKey: "phone")
        addParamUnlessNil(&params, param: newsletter, forKey: "newsletter")
        addParamUnlessNil(&params, param: name, forKey: "name")
                addParamUnlessNil(&params, param: description, forKey: "description")
        addParamUnlessNil(&params, param: location, forKey: "location")
        addParamUnlessNil(&params, param: notifiesInvite, forKey: "notifies_invite")
        addParamUnlessNil(&params, param: notifiesFriendRequest, forKey: "notifies_friend_request")
        addParamUnlessNil(&params, param: viewsMountainBiking, forKey: "views_mountain_biking")
        addParamUnlessNil(&params, param: viewsHiking, forKey: "views_hiking")
        
        YazdaAPI.Contstants.client.PUT(url, parameters: params, retry: true, success: { (op: AFHTTPRequestOperation!, object: AnyObject!) -> Void in
            
            NSLog("Getting account settings with params %@", params)
            
            success(object)
            
            }) { (op: AFHTTPRequestOperation!, error: NSError!) -> Void in
                
                NSLog("Failed getting account settings with params %@", params)
                
                failure(op, error)
        }
    }
    
    func updateProfileImage(image: NSData!, success: (AnyObject!)->Void, failure: (AFHTTPRequestOperation, NSError)->Void)
    {
        var url = buildURL(BASE_URL, params: "update_profile_image")
        var params = NSMutableDictionary(dictionary: [String : AnyObject]())
        
        let baseFile = image.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.EncodingEndLineWithLineFeed)
        
        addParamUnlessNil(&params, param: "data:image/png;base64,\(baseFile)", forKey: "avatar")
        
        YazdaAPI.Contstants.client.POST(url, parameters: params, retry: true, success: { (op: AFHTTPRequestOperation!, object: AnyObject!) -> Void in
            
            NSLog("Getting account settings")
            
            success(object)
            
            }) { (op: AFHTTPRequestOperation!, error: NSError!) -> Void in
                
                NSLog("Failed getting account settings")
                
                failure(op, error)
        }
    }
}