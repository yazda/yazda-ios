//
//  UserViewController.swift
//  yazda-ios
//
//  Created by James Stoup on 3/23/15.
//  Copyright (c) 2015 yazda. All rights reserved.
//

import UIKit
import AFNetworking
import FontAwesomeIconFactory

class UserViewController: BaseViewController {
    var user: NSDictionary?
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var friendRequestButton: UIButton!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var adventuresCreatedLabel: UILabel!
    @IBOutlet weak var followingButton: UIButton!
    @IBOutlet weak var followersButton: UIButton!
    @IBOutlet weak var descriptionText: UITextView!
    @IBOutlet weak var addFriendHeight: NSLayoutConstraint!
    
    @IBAction func friendRequest(sender: AnyObject) {
        let id = user?.objectForKey("id") as! Int
        
        self.disableActions()
        client!.friendships.create(id, success: { (data) -> Void in
            //TODO change image
            println("added friend")
            self.enableActions()
            
            self.friendRequestButton.hidden = true
            self.addFriendHeight.constant = 0
            
        }) { (op, error) -> Void in
            if let obj = op.responseObject as? NSDictionary {
                let messages = obj.objectForKey("errors") as! [String]
                
                YazdaAlert(parentView: self, messages: messages, withDelay: Common.Interval)
            }
            self.enableActions()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
 
                let id = user?.objectForKey("id") as! Int
        
        client?.users.show(id, success: { (obj) -> Void in
            let u = obj as! NSDictionary
            
            self.user = u.objectForKey("user") as? NSDictionary
        self.setUserInformation()
            }, failure: { (op: AFHTTPRequestOperation, error: NSError) -> Void in
                if let obj = op.responseObject as? NSDictionary {
                    let messages = obj.objectForKey("errors") as! [String]
                    
                    YazdaAlert(parentView: self, messages: messages, withDelay: Common.Interval)
                }

        })

        
        var factory = NIKFontAwesomeIconFactory.buttonIconFactory()
        factory.size = 15
        self.friendRequestButton.setImage(factory.createImageForIcon(NIKFontAwesomeIcon.UserPlus), forState: UIControlState.Normal)
        self.descriptionText.textAlignment = NSTextAlignment.Center
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUserInformation() {
        let url = self.user!.objectForKey("profile_image_url") as! String
        let imageUrl = NSURL(string: url)
        let followingCount = self.user!.objectForKey("following_count") as! Int
        let followersCount = self.user!.objectForKey("followers_count") as! Int
        let adventuresCount = self.user!.objectForKey("adventures_count") as! Int
        
        BackgroundTask.downloadUserProfileImage(&self.profileImage, url: imageUrl!)
        
        self.title = self.user!.objectForKey("name") as? String
        self.name.text = self.user!.objectForKey("name") as? String
        self.descriptionText.text = self.user!.objectForKey("description") as? String
        self.locationLabel.text = self.user!.objectForKey("location") as? String
        self.followingButton.setTitle("\(followingCount) following", forState: UIControlState.Normal)
        self.followersButton.setTitle("\(followersCount) followers", forState: UIControlState.Normal)
        self.adventuresCreatedLabel.text = "\(adventuresCount) adventures created"

        if (user!.objectForKey("following") as! Bool) == true {
            self.friendRequestButton.hidden = true
            self.addFriendHeight.constant = 0
        } else {
                    //TODO display another button if already sent a friend request
        }
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if let controller = segue.destinationViewController as? InviteFriendsTableViewController {
                controller.profileUserId = user!.objectForKey("id") as? Int
            
            if segue.identifier == "showFollowing" {
                controller.showFollowers = false
            } else if segue.identifier == "showFollowers" {
                controller.showFollowers = true
            }
        }
    }
}
