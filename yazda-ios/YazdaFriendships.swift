//
//  YazdaFriendships.swift
//  yazda-ios
//
//  Created by James Stoup on 3/14/15.
//  Copyright (c) 2015 yazda. All rights reserved.
//

import Foundation
import AFNetworking

class YazdaFriendships: YazdaBaseService {
    let BASE_URL = "/friendships"
    
    func incoming(page: Int?, pageSize: Int?, success: (AnyObject!)->Void, failure: (AFHTTPRequestOperation, NSError)->Void)
    {
        var params = NSMutableDictionary(dictionary: [String : AnyObject]())
        var url = buildURL(BASE_URL, params: "incoming")
        
        addParamUnlessNil(&params, param: page, forKey: "page")
        addParamUnlessNil(&params, param: pageSize, forKey: "page_size")
        
        YazdaAPI.Contstants.client.GET(url, parameters: params, retry: true, success: { (op: AFHTTPRequestOperation!, object: AnyObject!) -> Void in
            
            NSLog("Getting incoming friendships with params: %@", params)
            
            success(object)
            
            }) { (op: AFHTTPRequestOperation!, error: NSError!) -> Void in
                
                NSLog("Failed getting incoming friendships with params: %@",params)
                
                failure(op, error)
        }
    }
    
    func outgoing(page: Int?, pageSize: Int?, success: (AnyObject!)->Void, failure: (AFHTTPRequestOperation, NSError)->Void)
    {
        var params = NSMutableDictionary(dictionary: [String : AnyObject]())
        var url = buildURL(BASE_URL, params: "outgoing")
        
        addParamUnlessNil(&params, param: page, forKey: "page")
        addParamUnlessNil(&params, param: pageSize, forKey: "page_size")
        
        YazdaAPI.Contstants.client.GET(url, parameters: params, retry: true, success: { (op: AFHTTPRequestOperation!, object: AnyObject!) -> Void in
            
            NSLog("Getting outgoing friendships with params: %@", params)
            
            success(object)
            
            }) { (op: AFHTTPRequestOperation!, error: NSError!) -> Void in
                
                NSLog("Failed getting outgoing friendships with params: %@",params)
                
                failure(op, error)
        }
    }
    
    func accept(id: Int!, success: (AnyObject!)->Void, failure: (AFHTTPRequestOperation, NSError)->Void)
    {
        var url = buildURL(BASE_URL, params: String(id), "accept")
        
        YazdaAPI.Contstants.client.PUT(url, parameters: nil, retry: true, success: { (op: AFHTTPRequestOperation!, object: AnyObject!) -> Void in
            
            NSLog("Accepting friendship with id: %0d", id)
            
            success(object)
            
            }) { (op: AFHTTPRequestOperation!, error: NSError!) -> Void in
                
                NSLog("Failed accepting friendship with id: %0d", id)
                
                failure(op, error)
        }
    }

    func reject(id: Int!, success: (AnyObject!)->Void, failure: (AFHTTPRequestOperation, NSError)->Void)
    {
        var url = buildURL(BASE_URL, params: String(id), "reject")
        
        YazdaAPI.Contstants.client.PUT(url, parameters: nil, retry: true, success: { (op: AFHTTPRequestOperation!, object: AnyObject!) -> Void in
            
            NSLog("Rejecting friendship with id: %0d", id)
            
            success(object)
            
            }) { (op: AFHTTPRequestOperation!, error: NSError!) -> Void in
                
                NSLog("Failed rejecting friendship with id: %0d", id)
                
                failure(op, error)
        }
    }
    
    func create(userId: Int!, success: (AnyObject!)->Void, failure: (AFHTTPRequestOperation, NSError)->Void)
    {
        var url = buildURL(BASE_URL, params: "")
                var params = NSMutableDictionary(dictionary: [String : AnyObject]())
        
        addParamUnlessNil(&params, param: userId, forKey: "user_id")
        
        YazdaAPI.Contstants.client.POST(url, parameters: params, retry: true, success: { (op: AFHTTPRequestOperation!, object: AnyObject!) -> Void in
            
            NSLog("Creating friendship with userId: %0d", userId)
            
            success(object)
            
            }) { (op: AFHTTPRequestOperation!, error: NSError!) -> Void in
                
                NSLog("Failed creating friendship with userId: %0d", userId)
                
                failure(op, error)
        }
    }
}