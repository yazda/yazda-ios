//
//  LoginViewController.swift
//  yazda-ios
//
//  Created by James Stoup on 3/15/15.
//  Copyright (c) 2015 yazda. All rights reserved.
//

import UIKit

class LoginViewController: BaseViewController {
    
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    
    @IBAction func login(sender: AnyObject) {
        let emailText = email.text
        let passwordText = password.text
        
        self.disableActions()
        client!.session.login(emailText, password: passwordText,success: { () -> Void in
            self.enableActions()
            Common.navigateToLoggedInStoryboard()
            }) { (failure) -> Void in
                self.enableActions()
                
                let msg:String = failure as String
                
                YazdaAlert(parentView: self, messages: [msg], withDelay: Common.Interval)
        }
        
        self.email.resignFirstResponder()
        self.password.resignFirstResponder()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.email.delegate = self
        self.password.delegate = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
