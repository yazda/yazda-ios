//
//  YazdaUser.swift
//  yazda-ios
//
//  Created by James Stoup on 3/14/15.
//  Copyright (c) 2015 yazda. All rights reserved.
//

import Foundation
import AFNetworking

class YazdaUser: YazdaBaseService {
    let BASE_URL = "/users"
    
    func show(id: Int!, success: (AnyObject!)->Void, failure: (AFHTTPRequestOperation, NSError)->Void)
    {
        var url = buildURL(BASE_URL, params: String(id))
        
        YazdaAPI.Contstants.client.GET(url, parameters: nil, retry: true, success: { (op: AFHTTPRequestOperation!, object: AnyObject!) -> Void in
            
            NSLog("Getting User with id: %0d", id)
            
            success(object)
            
            }) { (op: AFHTTPRequestOperation!, error: NSError!) -> Void in
                
                NSLog("Failed getting user with id: %0d", id)
                
                failure(op, error)
        }
    }
    
    func search(query: String!, page: Int?, pageSize: Int?, success: (AnyObject!)->Void, failure: (AFHTTPRequestOperation, NSError)->Void)
    {
        var params = NSMutableDictionary(dictionary: [String : AnyObject]())
        var url = buildURL(BASE_URL, params: "search")
        
        addParamUnlessNil(&params, param: query, forKey: "q")
        addParamUnlessNil(&params, param: page, forKey: "page")
        addParamUnlessNil(&params, param: pageSize, forKey: "page_size")
        
        YazdaAPI.Contstants.client.GET(url, parameters: params, retry: true, success: { (op: AFHTTPRequestOperation!, object: AnyObject!) -> Void in
            
            NSLog("Searching User with params: %@", params)
            
            success(object)
            
            }) { (op: AFHTTPRequestOperation!, error: NSError!) -> Void in
                
                NSLog("Failed searching user with params: %@", params)
                
                failure(op, error)
        }
    }
}