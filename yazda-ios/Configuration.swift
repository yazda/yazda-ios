//
//  Configuration.swift
//  yazda-ios
//
//  Created by James Stoup on 3/28/15.
//  Copyright (c) 2015 yazda. All rights reserved.
//

import Foundation

let CONFIGURATION_PLIST = "ConfigurationPlist"

class Configuration: NSObject {
    private var store:NSDictionary?
    
    override init() {
        super.init()
        self.registerDefaultConfiguration()
    }
    
    class func defaultConfiguration() -> Configuration {
        return self.init()
    }
    
    func settingForKey(key: NSString) -> String {
        return self.store!.objectForKey(key) as! String
    }
    
    func registerDefaultConfiguration() {
        self.store = self.loadDefaults().mutableCopy() as? NSDictionary
    }
    
    func loadDefaults() -> NSDictionary {
        let plist = NSBundle.mainBundle().objectForInfoDictionaryKey(CONFIGURATION_PLIST) as! String
        let path = NSBundle.mainBundle().pathForResource(plist, ofType: "plist")
        
        return NSDictionary(contentsOfFile: path!)!
    }
}