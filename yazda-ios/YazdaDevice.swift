//
//  YazdaRegister.swift
//  yazda-ios
//
//  Created by James Stoup on 6/28/15.
//  Copyright (c) 2015 yazda. All rights reserved.
//

import Foundation
import AFNetworking

class YazdaDevice: YazdaBaseService {
    let BASE_URL = "/devices"
    
    func create(name: String!, token: String!, success: (AnyObject!)->Void, failure: (AFHTTPRequestOperation, NSError)->Void) {
        var url = buildURL(BASE_URL, params: "")
        var params = NSMutableDictionary(dictionary: [String : AnyObject]())
        
        addParamUnlessNil(&params, param: name, forKey: "name")
        addParamUnlessNil(&params, param: token, forKey: "identifier")
        
        YazdaAPI.Contstants.client.POST(url, parameters: params, retry: true, success: { (op: AFHTTPRequestOperation!, object: AnyObject!) -> Void in
            
            NSLog("Registering device with params: %@", params)
            
            success(object)
            
            }) { (op: AFHTTPRequestOperation!, error: NSError!) -> Void in
                NSLog("Failed to register device with params: %@", params)
                failure(op, error)
        }
    }
}