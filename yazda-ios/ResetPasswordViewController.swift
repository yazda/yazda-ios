//
//  ResetPasswordViewController.swift
//  yazda-ios
//
//  Created by James Stoup on 3/31/15.
//  Copyright (c) 2015 yazda. All rights reserved.
//

import UIKit
import AFNetworking

class ResetPasswordViewController: BaseViewController {
    @IBOutlet weak var email: YazdaTextField!
    @IBOutlet weak var message: UILabel!
    
    @IBAction func sendResetPassword(sender: AnyObject) {
        var emailText = email.text
        
        self.disableActions()
        
        client?.password.create(emailText, success: { (data) -> Void in
            self.enableActions()
            YazdaAlert(parentView: self, messages: ["Please check your email for instructions to reset your password"], withDelay: Common.Interval)
            
            }, failure: { (op: AFHTTPRequestOperation, error: NSError) -> Void in
                if let obj = op.responseObject as? NSDictionary {
                    let messages = obj.objectForKey("errors") as! [String]
                    
                    YazdaAlert(parentView: self, messages: messages, withDelay: Common.Interval)
                }
                self.enableActions()
        })
        
        self.email.resignFirstResponder()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.email.delegate = self
        self.message.hidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
