//
//  InvitesTableViewController.swift
//  yazda-ios
//
//  Created by James Stoup on 3/19/15.
//  Copyright (c) 2015 yazda. All rights reserved.
//

import UIKit
import AFNetworking

class InvitesTableViewController: BaseTableViewController {
    var currentPage = 1
    var totalPages = 0
    var selectedEvent:NSDictionary?
    var invites:NSMutableArray?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        self.invites = NSMutableArray()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.refresh()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        
        if(currentPage < totalPages)
        {
            return self.invites!.count + 1
        }
        
        return self.invites!.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if(indexPath.row < self.invites!.count)
        {
            return self.inviteCellForIndexPath(indexPath)
        } else {
            return self.loadingCell()
        }
    }
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if(cell.tag == kAnnotationType)
        {
            self.fetchInvites()
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        if let invite = self.invites?[indexPath.row] as? NSDictionary {
            if let adventure = invite.objectForKey("adventure") as? NSDictionary {
                client?.adventures.show(adventure.objectForKey("id") as! Int, success: { (obj) -> Void in
                    
                    let event = obj as? NSDictionary
                    
                    self.selectedEvent = event!.objectForKey("adventure") as? NSDictionary
                    
                    self.performSegueWithIdentifier("showEventFromInvite", sender: self)
                    
                    }, failure: { (op: AFHTTPRequestOperation, error: NSError) -> Void in
                        if let obj = op.responseObject as? NSDictionary {
                            let messages = obj.objectForKey("errors") as! [String]
                            
                            YazdaAlert(parentView: self, messages: messages, withDelay: Common.Interval)
                        }
                })
            }
        }
    }
    
    func refresh()
    {
        self.invites = NSMutableArray()
        self.currentPage = 1
        self.totalPages = 0
        self.fetchInvites()
    }
    
    private func inviteCellForIndexPath(indexPath: NSIndexPath) -> UITableViewCell
    {
        let invite = self.invites!.objectAtIndex(indexPath.row) as! NSDictionary
        let friendCellIdentifier = "friendInviteCell"
        let adventureCellIdentifier = "adventureInviteCell"
        
        if invite.objectForKey("friend") == nil {
            var cell = tableView.dequeueReusableCellWithIdentifier(adventureCellIdentifier) as? AdventureInviteTableViewCell
            
            if cell == nil {
                //Adventure Invite
                cell = AdventureInviteTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: adventureCellIdentifier)
                
            }
            
            cell!.setInviteData(invite)
            cell!.selectionStyle = UITableViewCellSelectionStyle.Default
            
            return cell!
        } else {
            var cell = tableView.dequeueReusableCellWithIdentifier(friendCellIdentifier) as? InviteTableViewCell
            
            if cell == nil {
                //Friend Invite
                cell = InviteTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: friendCellIdentifier)
            }
            
            cell!.setFriendshipData(invite, delegate: self)
            cell!.selectionStyle = UITableViewCellSelectionStyle.None
            
            return cell!
        }
    }
    
    private func loadingCell() -> UITableViewCell
    {
        var cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: nil)
        var activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.WhiteLarge)
        
        activityIndicator.center = cell.center
        cell.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        cell.tag = kAnnotationType
        
        return cell
    }
    
    private func fetchInvites()
    {
        self.disableActions()
        
        client!.invites.pending(self.currentPage, pageSize: nil, success: { (data: AnyObject!) -> Void in
            let objs = data as! NSDictionary!
            
            self.totalPages = objs.objectForKey("page_count") as! Int
            
            for obj in objs.objectForKey("invites") as! NSArray
            {
                let event = obj as! NSDictionary
                
                if(!self.invites!.containsObject(event))
                {
                    self.invites!.addObject(event)
                }
            }
            
            self.currentPage++
            self.tableView.reloadData()
            self.enableAction()
            
            badgeNumber = 0
            NSNotificationCenter.defaultCenter().postNotificationName("inviteNotification", object: nil, userInfo: nil)
            
            }) { (op: AFHTTPRequestOperation, error: NSError) -> Void in
                if let obj = op.responseObject as? NSDictionary {
                    let messages = obj.objectForKey("errors") as! [String]
                    
                    YazdaAlert(parentView: self, messages: messages, withDelay: Common.Interval)
                }
                
                self.enableAction()
        }
        
        client!.friendships.incoming(1, pageSize: nil, success: { (data) -> Void in
            let objs = data as! NSDictionary!
            
            for obj in objs.objectForKey("friendships") as! NSArray
            {
                let event = obj as! NSDictionary
                
                if(!self.invites!.containsObject(event))
                {
                    self.invites!.addObject(event)
                }
            }
            
            self.tableView.reloadData()
            self.enableAction()
            }) { (op: AFHTTPRequestOperation, error: NSError) -> Void in
                if let obj = op.responseObject as? NSDictionary {
                    let messages = obj.objectForKey("errors") as! [String]
                    
                    YazdaAlert(parentView: self, messages: messages, withDelay: Common.Interval)
                }
                
                self.enableAction()
        }
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        
        if(segue.identifier == "showEventFromInvite")
        {
            let controller = segue.destinationViewController as! EventViewController
            
            controller.event = self.selectedEvent
        }
    }
}
