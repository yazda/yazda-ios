//
//  EventTableViewCell.swift
//  yazda-ios
//
//  Created by James Stoup on 3/15/15.
//  Copyright (c) 2015 yazda. All rights reserved.
//

import UIKit
import MapKit

class EventTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    let cellIdentifier = "userCell"
    var users:NSMutableDictionary?
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var adventureType: UILabel!
    @IBOutlet weak var adventureTypeImage: UILabel!
    @IBOutlet weak var numberOfAdventurers: UILabel!
    @IBOutlet weak var adventureModifiers: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var friendContainer: FriendCollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.friendContainer.delegate = self
        self.friendContainer.dataSource = self
        
        self.adventureModifiers.font = UIFont(name: "icomoon", size: 25)
        self.adventureModifiers.textColor = Common.yazdaIconGray
        self.adventureModifiers.sizeToFit()
        self.users = NSMutableDictionary()
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setEventName(eventName: String!)
    {
        self.name.text = eventName
    }
    
    func setAdventureTypeLabel(type:String)
    {
        if(type == "mountain_biking")
        {
            self.adventureType.text = "Mountain Biking"
            
            self.adventureTypeImage.font = UIFont(name: "FontAwesome", size: 20)
            self.adventureTypeImage.text = Common.MOUNTAIN_BIKING
        } else if(type == "hiking") {
            self.adventureType.text = "Hiking"
            
            self.adventureTypeImage.font = UIFont(name: "icomoon", size: 20)
            self.adventureTypeImage.text = Common.HIKING
        }
    }
    
    func setDateLabel(startDate:String, endDate:String)
    {
        let startDateTime = Common.toDate(startDate)
        let endDateTime = Common.toDate(endDate)
        
        self.date.text = Common.formatTime(startDate)
        
        if Common.daysBetweenDate(startDateTime, toDateTime: endDateTime) > 1 {
            self.adventureModifiers.text = Common.MULTI_DAY
        }
    }
    
    func setInvites(invites: NSArray)
    {
        self.numberOfAdventurers.text = "\(invites.count + 1) Adventurers"
        users?.removeAllObjects()
        
        var count = 0;
        for invite in invites {
            var i = invite as! NSDictionary
            
            self.addFriend(i.objectForKey("user") as! NSDictionary)
            count++
            
            if count > 5 {
                break;
            }
        }
        
        self.friendContainer.reloadData()
    }
    
    func addFriend(user:NSDictionary) {
        var id = user.objectForKey("id") as! Int
        
        users?.setObject(user, forKey: id)
        
        self.friendContainer.reloadData()
    }
    
    func removeFriend(user:NSDictionary) {
        var id = user.objectForKey("id") as! Int
        
        users?.removeObjectForKey(id)
        
        self.friendContainer.reloadData()
        
    }
    
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        //#warning Incomplete method implementation -- Return the number of sections
        return 1
    }
    
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //#warning Incomplete method implementation -- Return the number of items in the section
        
        if self.users == nil {
            return 0
        } else {
            return users!.count
        }
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(cellIdentifier, forIndexPath: indexPath) as! UserCollectionViewCell
        let url = self.users?.allValues[indexPath.row].objectForKey("profile_image_thumb_url") as! String
        let imageUrl = NSURL(string: url)
        
        BackgroundTask.downloadUserProfileImage(&cell.profileImage, url: imageUrl!)
        
        return cell
    }
}
