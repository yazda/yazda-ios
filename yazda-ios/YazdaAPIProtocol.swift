//
//  YazdaAPIProtocol.swift
//  yazda-ios
//
//  Created by James Stoup on 3/13/15.
//  Copyright (c) 2015 yazda. All rights reserved.
//

import Foundation

protocol YazdaAPIProtocol {
    var BASE_URL:String { get }
}