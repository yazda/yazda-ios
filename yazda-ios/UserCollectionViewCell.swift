//
//  UserCollectionViewCell.swift
//  yazda-ios
//
//  Created by James Stoup on 3/23/15.
//  Copyright (c) 2015 yazda. All rights reserved.
//

import UIKit

class UserCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var profileImage: UIImageView!
}
