//
//  YazdaAlert.swift
//  yazda-ios
//
//  Created by James Stoup on 5/1/15.
//  Copyright (c) 2015 yazda. All rights reserved.
//

import UIKit
import FontAwesomeIconFactory

class YazdaAlert: UIAlertController {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

    convenience init(parentView: UIViewController, messages: [String], withDelay: NSTimeInterval) {
        self.init()
        self.title = "Error"
        
        var factory = NIKFontAwesomeIconFactory.buttonIconFactory()
        var view = UIImageView(frame: CGRectMake(0,0,45,45))
        
        factory.size = 25
        factory.colors = [Common.yazdaAlertOrange]
        
        view.image = factory.createImageForIcon(NIKFontAwesomeIcon.ExclamationCircle)
        view.contentMode = UIViewContentMode.Center
        self.view.tintColor = Common.yazdaOrange
        self.view.addSubview(view)
        
        for message in messages {
            self.addAction(UIAlertAction(title: message, style: UIAlertActionStyle.Default, handler: nil))
        }
        
        parentView.presentViewController(self, animated: true) { () -> Void in
            NSTimer.scheduledTimerWithTimeInterval(withDelay, target: self, selector: "dismissAfterDelay", userInfo: nil, repeats: false)
        }
    }
    
    func dismissAfterDelay() {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}
