//
//  YazdaPassword.swift
//  yazda-ios
//
//  Created by James Stoup on 4/4/15.
//  Copyright (c) 2015 yazda. All rights reserved.
//

import Foundation
import AFNetworking

class YazdaPassword: YazdaBaseService {
    let BASE_URL = "/reset_password"
    
    func create(email: String!, success: (AnyObject!)->Void, failure: (AFHTTPRequestOperation, NSError)->Void) {
        var url = buildURL(BASE_URL, params: "")
        var params = NSMutableDictionary(dictionary: [String : AnyObject]())
        
        addParamUnlessNil(&params, param: email, forKey: "email")
        
        YazdaAPI.Contstants.client.POST(url, parameters: params, retry: true, success: { (op: AFHTTPRequestOperation!, object: AnyObject!) -> Void in
            
            NSLog("Resetting password with params: %@", params)
            
            success(object)
            
            }) { (op: AFHTTPRequestOperation!, error: NSError!) -> Void in
                
                NSLog("Failed resetting password with params: %@", params)
                
                failure(op, error)
        }
    }
    
    func update(token: String!, password: String!, passwordConfirmation: String!, success: (AnyObject!)->Void, failure: (AFHTTPRequestOperation, NSError)->Void) {
        var url = buildURL(BASE_URL, params: "")
        var params = NSMutableDictionary(dictionary: [String : AnyObject]())
        
        addParamUnlessNil(&params, param: token, forKey: "reset_password_token")
        addParamUnlessNil(&params, param: password, forKey: "password")
        addParamUnlessNil(&params, param: passwordConfirmation, forKey: "password_confirmation")
        
        YazdaAPI.Contstants.client.PUT(url, parameters: params, retry: true, success: { (op: AFHTTPRequestOperation!, object: AnyObject!) -> Void in
            
            NSLog("Changing reset password")
            
            success(object)
            
            }) { (op: AFHTTPRequestOperation!, error: NSError!) -> Void in
                
                NSLog("Failed changing resetting password")
                
                failure(op, error)
        }
    }
}