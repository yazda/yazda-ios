//
//  BaseViewController.swift
//  yazda-ios
//
//  Created by James Stoup on 3/31/15.
//  Copyright (c) 2015 yazda. All rights reserved.
//

import UIKit

class BaseViewController:  UIViewController, UITextFieldDelegate, UITextViewDelegate {
    var activeField:UITextField?
    var activeTextField:UITextView?
    var activityView: UIActivityIndicatorView?
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidAppear(animated: Bool) {
        if(self.scrollView != nil)
        {
            self.automaticallyAdjustsScrollViewInsets = false
            
            NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardDidShow:", name: UIKeyboardDidShowNotification, object: nil)
            NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillBeHidden:", name: UIKeyboardWillHideNotification, object: nil)
            
            self.scrollView.keyboardDismissMode = UIScrollViewKeyboardDismissMode.OnDrag
        }
        
        super.viewDidAppear(true)
    }
    
    override func viewWillDisappear(animated: Bool) {
        self.activeTextField?.resignFirstResponder()
        self.activeField?.resignFirstResponder()
    }
    
    override func viewDidDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self)
        
        super.viewDidDisappear(true)
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        self.view.endEditing(true);
    }
    
    func enableActions() {
        self.activityView?.stopAnimating()
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
    }
    
    func disableActions() {
        self.activityView = UIActivityIndicatorView(frame: CGRectMake(0,0,100,100))
        self.activityView!.center = self.view.center
        self.activityView!.hidesWhenStopped = true
        self.activityView!.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
        self.view.addSubview(self.activityView!)
        self.activityView!.startAnimating()
        
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
    }
    
    func textFieldShouldReturn(field: UITextField) -> Bool
    {
        field.resignFirstResponder()
        
        return true
    }
    
    func textFieldDidBeginEditing(sender: UITextField)
    {
        self.activeField = sender
    }
    
    func textFieldDidEndEditing(sender: UITextField)
    {
        self.activeField = nil
    }
    
    func textViewDidBeginEditing(textView: UITextView) {
        self.activeTextField = textView
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        self.activeTextField = nil
    }
    
    func keyboardDidShow(notification: NSNotification)
    {
        if((self.activeField != nil || self.activeTextField != nil) && self.scrollView != nil) {
            if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
                let contentInsets = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height, right: 0)
                
                self.scrollView!.contentInset = contentInsets
                self.scrollView!.scrollIndicatorInsets = contentInsets
                
                var aRect = self.view.frame
                aRect.size.height -= keyboardSize.size.height
                
                if(self.activeField != nil) {
                    if(!CGRectContainsPoint(aRect, self.activeField!.frame.origin)) {
                        self.scrollView!.scrollRectToVisible(self.activeField!.frame, animated: true)
                    }
                } else if(self.activeTextField != nil) {
                    if(!CGRectContainsPoint(aRect, self.activeTextField!.frame.origin)) {
                        self.scrollView!.scrollRectToVisible(self.activeTextField!.frame, animated: true)
                    }
                }
            }
        }
    }
    
    func keyboardWillBeHidden(notification: NSNotification)
    {
        if(self.scrollView != nil) {
            let contentInsets:UIEdgeInsets = UIEdgeInsetsZero
            
            self.scrollView!.contentInset = contentInsets
            self.scrollView!.scrollIndicatorInsets = contentInsets
        }
    }
}
