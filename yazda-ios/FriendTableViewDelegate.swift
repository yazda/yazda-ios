//
//  FriendTableViewDelegate.swift
//  yazda-ios
//
//  Created by James Stoup on 3/16/15.
//  Copyright (c) 2015 yazda. All rights reserved.
//

import UIKit

protocol FriendTableViewDelegate {
    func friendTableViewControllerAddFriend(controller: UIViewController, user: NSDictionary!)
    func friendTableViewControllerRemoveFriend(controller: UIViewController, user: NSDictionary!)
}
