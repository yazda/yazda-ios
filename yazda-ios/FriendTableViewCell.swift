//
//  FriendTableViewCell.swift
//  yazda-ios
//
//  Created by James Stoup on 3/16/15.
//  Copyright (c) 2015 yazda. All rights reserved.
//

import UIKit

class FriendTableViewCell: UITableViewCell {
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    func setName(name: String!)
    {
        self.userName.text = name
    }
    
    func setFriendImage(image: UIImage)
    {
        self.userImage.image = image
    }
    
}
