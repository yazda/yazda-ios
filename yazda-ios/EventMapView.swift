//
//  EventMapView.swift
//  yazda-ios
//
//  Created by James Stoup on 3/15/15.
//  Copyright (c) 2015 yazda. All rights reserved.
//

import UIKit
import MapKit

class EventMapView: MKMapView {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

    override func pointInside(point: CGPoint, withEvent event: UIEvent?) -> Bool
    {
        return false
    }
}
