//
//  FriendCollectionViewController.swift
//  yazda-ios
//
//  Created by James Stoup on 3/23/15.
//  Copyright (c) 2015 yazda. All rights reserved.
//

import UIKit

class FriendCollectionViewController: UICollectionViewController {
    let reuseIdentifier = "userImageCell"
    
    var users:NSMutableDictionary = NSMutableDictionary()
    var selectedUser:NSDictionary?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "showFriend"
        {
            if let controller = segue.destinationViewController as? UserViewController
            {
                controller.user = self.selectedUser
            }
        }
    }

    // MARK: UICollectionViewDataSource

    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        //#warning Incomplete method implementation -- Return the number of sections
        return 1
    }


    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //#warning Incomplete method implementation -- Return the number of items in the section
        return users.count
    }

    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! UserCollectionViewCell
        let imageUrl = NSURL(string: users.allValues[indexPath.row].objectForKey("profile_image_thumb_url") as! String)
        
        BackgroundTask.downloadUserProfileImage(&cell.profileImage, url: imageUrl!)
        return cell
    }

    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        self.selectedUser = users.allValues[indexPath.row] as? NSDictionary
        
        self.performSegueWithIdentifier("showFriend", sender: self)
    }
    
    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(collectionView: UICollectionView, shouldHighlightItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(collectionView: UICollectionView, shouldSelectItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(collectionView: UICollectionView, shouldShowMenuForItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, canPerformAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, performAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) {
    
    }
    */

}

extension FriendCollectionViewController: FriendCollectionViewDelegate {
    func friendCollectionViewDelegateAddFriend(user: NSDictionary!) {
        var id = user.objectForKey("id") as! Int
        
        users.setObject(user, forKey: id)
        
        self.collectionView!.reloadData()
    }
    
    func friendCollectionViewDelegateRemoveFriend(user: NSDictionary!) {
        var id = user.objectForKey("id") as! Int
        
        users.removeObjectForKey(id)
        
        self.collectionView!.reloadData()
    }
}
