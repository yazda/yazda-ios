//
//  InviteFriendsTableViewController.swift
//  yazda-ios
//
//  Created by James Stoup on 3/16/15.
//  Copyright (c) 2015 yazda. All rights reserved.
//

import UIKit
import AFNetworking

class InviteFriendsTableViewController: BaseTableViewController, UISearchBarDelegate, UISearchResultsUpdating {
    var delegate:FriendTableViewDelegate?
    var friends:NSMutableArray!
    var currentPage = 1
    var totalPages = 0
    var selectedEvent:NSDictionary?
    var userIds:NSMutableSet?
    
    var profileUserId:Int?
    var selectedUser:NSDictionary?
    var isInviteFriends:Bool?
    var showFollowers:Bool?
    var isSearch:Bool?
    
    var searchController:UISearchController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var defaults = NSUserDefaults.standardUserDefaults()
        
        if self.profileUserId == nil {
            self.profileUserId = defaults.integerForKey("currentUserId")
        }
        
        self.friends = NSMutableArray(array: [NSDictionary]())
        self.fetchFriends()
        
        self.searchController = UISearchController(searchResultsController: nil)
        self.searchController!.searchResultsUpdater = self
        self.searchController!.dimsBackgroundDuringPresentation = false
        self.searchController?.hidesNavigationBarDuringPresentation = false
        self.searchController!.searchBar.delegate = self
        
        if(isSearch == true) {
            self.tableView.tableHeaderView = self.searchController!.searchBar
            self.searchController?.searchBar.sizeToFit()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        client?.users.search(searchController.searchBar.text, page: currentPage, pageSize: nil, success: { (obj) -> Void in
            var users = obj as! NSDictionary
            
            self.friends = users.objectForKey("users") as! NSMutableArray
            self.tableView.reloadData()
            
            }, failure: { (op: AFHTTPRequestOperation, error: NSError) -> Void in
            //TODO show error message
        })
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        if(currentPage == 0)
        {
            return 1
        }
        
        if(currentPage < totalPages)
        {
            return self.friends.count + 1
        }
        
        return self.friends.count
        
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if(indexPath.row < self.friends.count)
        {
            return self.inviteCellForIndexPath(indexPath)
        } else {
            return self.loadingCell()
        }
    }
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if(cell.tag == kAnnotationType)
        {
            currentPage++
            self.fetchFriends()
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var cell:UITableViewCell = tableView.cellForRowAtIndexPath(indexPath)!
        
        let user = self.friends[indexPath.row] as! NSDictionary
        
        if isInviteFriends == true {
            //TODO get event by id to get all invites otherwise error
            
            if cell.accessoryType == UITableViewCellAccessoryType.Checkmark
            {
                cell.accessoryType = UITableViewCellAccessoryType.None
                self.delegate?.friendTableViewControllerRemoveFriend(self, user: user)
            } else {
                cell.accessoryType = UITableViewCellAccessoryType.Checkmark
                self.delegate?.friendTableViewControllerAddFriend(self, user: user)
            }
        } else {
            self.selectedUser = user
            performSegueWithIdentifier("showUser", sender: self)
        }
    }
    
    func refresh()
    {
        self.currentPage = 1
        self.friends.removeAllObjects()
        self.totalPages = 0
        self.fetchFriends()
    }
    
    private func inviteCellForIndexPath(indexPath: NSIndexPath) -> UITableViewCell
    {
        let cellIdentifier = "inviteFriendCell"
        var cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as? FriendTableViewCell
        
        if(cell == nil)
        {
            cell = FriendTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdentifier)
        }
        
        let friend = self.friends.objectAtIndex(indexPath.row) as! NSDictionary
        let id = friend.objectForKey("id") as! Int!
        let imageUrl = NSURL(string: friend.objectForKey("profile_image_thumb_url") as! String)
        
        if self.isInviteFriends == true {
            if(self.userIds!.containsObject(id))
            {
                cell!.accessoryType = UITableViewCellAccessoryType.Checkmark
            }
        }
        
        cell!.setName(friend.objectForKey("name") as? String)
        BackgroundTask.downloadUserProfileImage(&cell!.userImage, url: imageUrl!)
        
        return cell!
    }
    
    private func loadingCell() -> UITableViewCell
    {
        var cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: nil)
        var activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.WhiteLarge)
        
        activityIndicator.center = cell.center
        cell.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        cell.tag = kAnnotationType
        
        return cell
    }
    
    private func fetchFriends()
    {
        self.disableActions()
        
        let successClosure = { (data: AnyObject!) -> Void in
            let objs = data as! NSDictionary!
            
            self.totalPages = objs.objectForKey("page_count") as! Int
            
            for obj in objs.objectForKey("users") as! NSArray
            {
                let friend = obj as! NSDictionary
                
                if(!self.friends.containsObject(friend))
                {
                    self.friends.addObject(friend)
                }
            }
            

            self.tableView.reloadData()
            self.enableAction()
        }
        
        let failureClosure = { (op: AFHTTPRequestOperation, error: NSError) -> Void in
            if let obj = op.responseObject as? NSDictionary {
                let messages = obj.objectForKey("errors") as! [String]
                
                YazdaAlert(parentView: self, messages: messages, withDelay: Common.Interval)
            }
            
            self.enableAction()
        }
        
        if self.showFollowers == true {
            client!.followers.index(self.profileUserId, page: currentPage, pageSize: nil, success: successClosure, failure: failureClosure)
        } else {
            client!.friends.index(self.profileUserId, page: currentPage, pageSize: nil, success: successClosure, failure: failureClosure)
        }
    }
    
    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    if editingStyle == .Delete {
    // Delete the row from the data source
    tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
    } else if editingStyle == .Insert {
    // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
    }
    */
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "showUser" {
            let controller = segue.destinationViewController as! UserViewController
            
            controller.user = self.selectedUser
        }
    }
}
