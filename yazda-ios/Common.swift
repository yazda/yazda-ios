//
//  Common.swift
//  yazda-ios
//
//  Created by James Stoup on 3/13/15.
//  Copyright (c) 2015 yazda. All rights reserved.
//

import UIKit
import Foundation

struct Common {
    static let inputDateFormatter:NSDateFormatter = NSDateFormatter()
    static let outputDateFormatter:NSDateFormatter = NSDateFormatter()
    
    static let yazdaIconGray:UIColor = UIColor(red:0.667, green:0.667, blue:0.667, alpha:1)
    static let yazdaIconBackgroundGray:UIColor = UIColor(red:0.945, green:0.945, blue:0.945, alpha:1)
    static let yazdaGray:UIColor = UIColor(red: 0.796, green: 0.788, blue: 0.788, alpha: 1)
    static let yazdaBlack:UIColor = UIColor.blackColor()
    static let yazdaOrange:UIColor = UIColor(red: 0.949, green: 0.396, blue: 0.188, alpha: 1)
        static let yazdaAlertOrange:UIColor = UIColor(red:0.667, green:0.667, blue:0.667, alpha:1)
    static let yazdaGreen:UIColor = UIColor(red:0.161, green:0.596, blue:0.333, alpha:1)
    static let yazdaDarkGreen:UIColor = UIColor(red:0.075, green:0.416, blue:0.208, alpha:1)
    
    static let HIKING =  "\u{e600}"
        static let MULTI_DAY =  "\u{e601}"
    static let MOUNTAIN_BIKING = "\u{f206}"
    
    static let Interval:NSTimeInterval = 2
    
    static func navigateToLoggedInStoryboard()
    {
        let storyboard = UIStoryboard(name: "LoggedIn", bundle: nil)
        let mainController = storyboard.instantiateViewControllerWithIdentifier("YazdaTabBarController") as! UITabBarController
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        appDelegate.window!.rootViewController = mainController
    }
    
    static func navigateToMainStoryboard()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mainController = storyboard.instantiateViewControllerWithIdentifier("ViewController") as! UIViewController
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = mainController
    }
    
    static func registerForNotifications() {
        let inviteCategory = UIMutableUserNotificationCategory()
        
        inviteCategory.setActions(nil, forContext: UIUserNotificationActionContext.Default)
        inviteCategory.setActions(nil, forContext: UIUserNotificationActionContext.Minimal)
        
        let settings = UIUserNotificationSettings(forTypes: UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound, categories: NSSet(object: inviteCategory) as Set<NSObject>)
        
        UIApplication.sharedApplication().registerUserNotificationSettings(settings)
        UIApplication.sharedApplication().registerForRemoteNotifications()
    }
    
    static func setupUILookAndFeel()
    {
        YazdaButton.appearance().setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        
        UIButton.appearance().setTitleColor(yazdaOrange, forState: UIControlState.Normal)
        
        UISegmentedControl.appearance().tintColor = Common.yazdaOrange
        
        UINavigationBar.appearance().barTintColor = Common.yazdaGreen
        UINavigationBar.appearance().tintColor = UIColor.whiteColor()
        UINavigationBar.appearance().translucent = false
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor()]
        
        UISwitch.appearance().onTintColor = Common.yazdaGreen
        
        UIApplication.sharedApplication().setStatusBarStyle(UIStatusBarStyle.LightContent, animated: true)
        
        UITabBar.appearance().tintColor = Common.yazdaOrange
    }
    
    static func formatTime(date:String) -> String {
        self.inputDateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        self.outputDateFormatter.dateFormat = "h:mm a"
        
        let myDate:NSDate = self.inputDateFormatter.dateFromString(date)!
        
        return self.outputDateFormatter.stringFromDate(myDate)
    }
    
    static func formatDateAndTime(date:String) -> String {
        self.inputDateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        
        let theDate:NSDate = self.inputDateFormatter.dateFromString(date)!
        
        return self.formatDateAndTime(theDate)
    }
    
    static func formatDateAndTime(date:NSDate) -> String {
        self.outputDateFormatter.dateFormat = "MMM d h:mm a"
        
        return self.outputDateFormatter.stringFromDate(date)
    }
    
    static func toDate(dateString: String) -> NSDate {
        self.inputDateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"

        return self.inputDateFormatter.dateFromString(dateString)!
    }
    
    static func toDateString(date: NSDate) -> String {
        if(date.isEqualToDateIgnoringTime(NSDate()))
        {
            return "Today"
        } else {
            self.outputDateFormatter.dateFormat = "MMMM d"
            
            return self.outputDateFormatter.stringFromDate(date)
        }
    }
    
    static func daysBetweenDate(fromDateTime: NSDate, toDateTime: NSDate) -> NSInteger{
        var calendar = NSCalendar.currentCalendar()
        var fromDate:NSDate?
        var toDate:NSDate?
        
        calendar.rangeOfUnit(NSCalendarUnit.DayCalendarUnit, startDate: &fromDate, interval: nil, forDate: fromDateTime)
                calendar.rangeOfUnit(NSCalendarUnit.DayCalendarUnit, startDate: &toDate, interval: nil, forDate: toDateTime)
        
        var difference:NSDateComponents = calendar.components(NSCalendarUnit.DayCalendarUnit, fromDate: fromDate!, toDate: toDate!, options: nil)
        
        return difference.day
    }
}

extension NSDate {
    func timeAgoSinceDate(numericDates:Bool) -> String {
        let calendar = NSCalendar.currentCalendar()
        let unitFlags = NSCalendarUnit.CalendarUnitMinute | NSCalendarUnit.CalendarUnitHour | NSCalendarUnit.CalendarUnitDay | NSCalendarUnit.CalendarUnitWeekOfYear | NSCalendarUnit.CalendarUnitMonth | NSCalendarUnit.CalendarUnitYear | NSCalendarUnit.CalendarUnitSecond
        let now = NSDate()
        let earliest = now.earlierDate(self)
        let latest = (earliest == now) ? self : now
        let components:NSDateComponents = calendar.components(unitFlags, fromDate: earliest, toDate: latest, options: nil)
        
        if (components.year >= 2) {
            return "\(components.year) years ago"
        } else if (components.year >= 1){
            if (numericDates){
                return "1 year ago"
            } else {
                return "Last year"
            }
        } else if (components.month >= 2) {
            return "\(components.month) months ago"
        } else if (components.month >= 1){
            if (numericDates){
                return "1 month ago"
            } else {
                return "Last month"
            }
        } else if (components.weekOfYear >= 2) {
            return "\(components.weekOfYear) weeks ago"
        } else if (components.weekOfYear >= 1){
            if (numericDates){
                return "1 week ago"
            } else {
                return "Last week"
            }
        } else if (components.day >= 2) {
            return "\(components.day) days ago"
        } else if (components.day >= 1){
            if (numericDates){
                return "1 day ago"
            } else {
                return "Yesterday"
            }
        } else if (components.hour >= 2) {
            return "\(components.hour) hours ago"
        } else if (components.hour >= 1){
            if (numericDates){
                return "1 hour ago"
            } else {
                return "An hour ago"
            }
        } else if (components.minute >= 2) {
            return "\(components.minute) minutes ago"
        } else if (components.minute >= 1){
            if (numericDates){
                return "1 minute ago"
            } else {
                return "A minute ago"
            }
        } else if (components.second >= 3) {
            return "\(components.second) seconds ago"
        } else {
            return "Just now"
        }
        
    }
    
    static func dateAtBeginningOfDayForDate(inputDate: NSDate) -> NSDate
    {
        var calendar = NSCalendar.currentCalendar()
        var timeZone = NSTimeZone.systemTimeZone()

        calendar.timeZone = timeZone

        var dateComponents = calendar.components(NSCalendarUnit.YearCalendarUnit | NSCalendarUnit.MonthCalendarUnit | NSCalendarUnit.DayCalendarUnit, fromDate: inputDate)
        
        dateComponents.hour = 0
        dateComponents.minute = 0
        dateComponents.second = 0
        
        return calendar.dateFromComponents(dateComponents)!
    }
    
    func isEqualToDateIgnoringTime(date: NSDate) -> Bool
    {
        let comp1 = NSDate.components(fromDate: self)
        let comp2 = NSDate.components(fromDate: date)
        return ((comp1.year == comp2.year) && (comp1.month == comp2.month) && (comp1.day == comp2.day))
    }
    
    // MARK: Components
    private class func componentFlags() -> NSCalendarUnit { return NSCalendarUnit.CalendarUnitYear | NSCalendarUnit.CalendarUnitMonth | NSCalendarUnit.CalendarUnitDay | NSCalendarUnit.CalendarUnitWeekOfYear | NSCalendarUnit.CalendarUnitHour | NSCalendarUnit.CalendarUnitMinute | NSCalendarUnit.CalendarUnitSecond  | NSCalendarUnit.CalendarUnitWeekday | NSCalendarUnit.CalendarUnitWeekdayOrdinal | NSCalendarUnit.CalendarUnitWeekOfYear }
    
    private class func components(#fromDate: NSDate) -> NSDateComponents! {
        return NSCalendar.currentCalendar().components(NSDate.componentFlags(), fromDate: fromDate)
    }
    
    private func components() -> NSDateComponents  {
        return NSDate.components(fromDate: self)!
    }
}
