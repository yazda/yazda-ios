//
//  YazdaTextView.swift
//  yazda-ios
//
//  Created by James Stoup on 4/24/15.
//  Copyright (c) 2015 yazda. All rights reserved.
//

import UIKit

class YazdaTextView: UITextView {

    required init(coder aDecoder:NSCoder) {
        super.init(coder: aDecoder)
        
        self.layer.borderWidth = 1
        self.layer.borderColor = Common.yazdaGray.CGColor
    }
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
