//
//  SetLocationViewDelegate.swift
//  yazda-ios
//
//  Created by James Stoup on 3/17/15.
//  Copyright (c) 2015 yazda. All rights reserved.
//

import UIKit
import CoreLocation

protocol SetLocationViewDelegate {
    func setLocationViewDelegateSelectLoction(location: CLLocationCoordinate2D!, address: String!)
}
