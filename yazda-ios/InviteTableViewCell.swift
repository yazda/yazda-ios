//
//  InviteTableViewCell.swift
//  yazda-ios
//
//  Created by James Stoup on 3/19/15.
//  Copyright (c) 2015 yazda. All rights reserved.
//

import UIKit
import AFNetworking

class InviteTableViewCell: UITableViewCell {
    var friendship:NSDictionary?
    var table:InvitesTableViewController?
    
    @IBOutlet weak var ownerImage: UIImageView!
    @IBOutlet weak var timeAgoLabel: UILabel!
    @IBOutlet weak var invitedByLabel: UILabel!

    @IBAction func selectedInvite(sender: UISegmentedControl) {
        let id = friendship?.objectForKey("id") as! Int
        
        if sender.selectedSegmentIndex == 0 {
            client?.friendships.accept(id, success: { (obj) -> Void in
                //TODO this isn't working
                self.table?.refresh()
                
                }, failure: { (op: AFHTTPRequestOperation, error: NSError) -> Void in
                    if let obj = op.responseObject as? NSDictionary {
                        let messages = obj.objectForKey("errors") as! [String]
                        
                        //TODO display allert
//                        YazdaAlert(parentView: self, messages: messages, withDelay: Common.Interval)
                    }
            })
        } else {
            client?.friendships.reject(id, success: { (obj) -> Void in
                //TODO this isn't working
                self.table?.refresh()
                
                }, failure: { (op: AFHTTPRequestOperation, error: NSError) -> Void in
                    if let obj = op.responseObject as? NSDictionary {
                        let messages = obj.objectForKey("errors") as! [String]
                        
                        //TODO display allert
                        //                        YazdaAlert(parentView: self, messages: messages, withDelay: Common.Interval)
                    }

            })
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setFriendshipData(ship: NSDictionary, delegate: InvitesTableViewController) {
        self.friendship = ship
        
        let user = self.friendship?.objectForKey("user") as! NSDictionary
        let url = user.objectForKey("profile_image_thumb_url") as! String
        let name = user.objectForKey("name") as! String
        let dateString = self.friendship?.objectForKey("created_at") as! String
        let date = Common.toDate(dateString)
        
        BackgroundTask.downloadUserProfileImage(&self.ownerImage, url: NSURL(string: url)!)
        
        self.invitedByLabel.text = "\(name) sent you a friend request."
        self.timeAgoLabel.text = date.timeAgoSinceDate(false)
        self.table = delegate
    }
}
