//
//  YazdaButton.swift
//  yazda-ios
//
//  Created by James Stoup on 4/3/15.
//  Copyright (c) 2015 yazda. All rights reserved.
//

import UIKit

class YazdaButton: UIButton {
    let padding = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    
    required init(coder aDecoder:NSCoder) {
        super.init(coder: aDecoder)
        
        self.layer.cornerRadius = 4
        self.layer.borderColor = Common.yazdaGray.CGColor
        self.layer.borderWidth = 0.2
        self.backgroundColor = Common.yazdaOrange
        self.tintColor = UIColor.whiteColor()
        self.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
    }
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
    // Drawing code
    }
    */
}
