//
//  YazdaInvites.swift
//  yazda-ios
//
//  Created by James Stoup on 3/15/15.
//  Copyright (c) 2015 yazda. All rights reserved.
//

import Foundation
import AFNetworking

class YazdaInvites: YazdaBaseService {
    let BASE_URL = "/invites"
    
    func pending(page: Int?, pageSize: Int?, success: (AnyObject!)->Void, failure: (AFHTTPRequestOperation, NSError)->Void)
    {
        var params = NSMutableDictionary(dictionary: [String : AnyObject]())
        var url = buildURL(BASE_URL, params: "pending")
        
        addParamUnlessNil(&params, param: page, forKey: "page")
        addParamUnlessNil(&params, param: pageSize, forKey: "page_size")
        
        YazdaAPI.Contstants.client.GET(url, parameters: params, retry: true, success: { (op: AFHTTPRequestOperation!, object: AnyObject!) -> Void in
            
            NSLog("Getting pending invites with params: %@", params)
            
            success(object)
            
            }) { (op: AFHTTPRequestOperation!, error: NSError!) -> Void in
                
                NSLog("Failed getting pending invites with params: %@", params)
                
                failure(op, error)
        }
    }
    
    func accept(adventure_id: Int!, success: (AnyObject!)->Void, failure: (AFHTTPRequestOperation, NSError)->Void)
    {
                var url = buildURL(BASE_URL, params: "accept?adventure_id=\(adventure_id)")
        
        YazdaAPI.Contstants.client.PUT(url, parameters: nil, retry: true, success: { (op: AFHTTPRequestOperation!, object: AnyObject!) -> Void in
            
            NSLog("Accepting invite with adventure_id: %0d", adventure_id)
            
            success(object)
            
            }) { (op: AFHTTPRequestOperation!, error: NSError!) -> Void in
                
                NSLog("Failed accepting invite with adventure_id: %0d", adventure_id)
                
                failure(op, error)
        }
    }
    
    func reject(adventure_id: Int!, success: (AnyObject!)->Void, failure: (AFHTTPRequestOperation, NSError)->Void)
    {
        var url = buildURL(BASE_URL, params: "reject?adventure_id=\(adventure_id)")
        
        YazdaAPI.Contstants.client.PUT(url, parameters: nil, retry: true, success: { (op: AFHTTPRequestOperation!, object: AnyObject!) -> Void in
            
            NSLog("Rejecting invite with adventure_id: %0d", adventure_id)
            
            success(object)
            
            }) { (op: AFHTTPRequestOperation!, error: NSError!) -> Void in
                
                NSLog("Failed rejecting invite with adventure_id: %0d", adventure_id)
                
                failure(op, error)
        }
    }
}