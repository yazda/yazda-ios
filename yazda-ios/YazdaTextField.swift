//
//  YazdaTextField.swift
//  yazda-ios
//
//  Created by James Stoup on 4/3/15.
//  Copyright (c) 2015 yazda. All rights reserved.
//

import UIKit
import FontAwesomeIconFactory

class YazdaTextField: UITextField {
    let padding = UIEdgeInsets(top: 10, left: 15, bottom: 10, right: 15)
    
    required init(coder aDecoder:NSCoder) {
        super.init(coder: aDecoder)
        
        self.borderStyle = UITextBorderStyle.None
        self.layer.cornerRadius = 5
        self.layer.borderColor = Common.yazdaGray.CGColor
        self.layer.borderWidth = 1.5
        self.textColor = Common.yazdaBlack
        self.backgroundColor = UIColor.whiteColor()
    }
    
    func addIcon(icon: NIKFontAwesomeIcon)
    {
        var factory = NIKFontAwesomeIconFactory.buttonIconFactory()
        var eventNameIcon = UIImageView(frame: CGRectMake(0, 0, 50, 50))
        
        factory.colors = [Common.yazdaIconGray]
        factory.edgeInsets = NIKEdgeInsets(top: 12, left: 12, bottom: 12, right: 12)
        
        eventNameIcon.backgroundColor = Common.yazdaIconBackgroundGray
        eventNameIcon.image = factory.createImageForIcon(icon)
        
        self.leftViewMode = UITextFieldViewMode.Always
        self.leftView = eventNameIcon
    }
    
    override func textRectForBounds(bounds: CGRect) -> CGRect {
        return self.newBounds(bounds)
    }
    
    override func placeholderRectForBounds(bounds: CGRect) -> CGRect {
        return self.newBounds(bounds)
    }
    
    override func editingRectForBounds(bounds: CGRect) -> CGRect {
        return self.newBounds(bounds)
    }
    
    private func newBounds(bounds: CGRect) -> CGRect {
        var newBounds = bounds
        
        if(self.leftViewMode != UITextFieldViewMode.Always)
        {
            newBounds.origin.x += padding.left
            newBounds.size.width -= (padding.left + padding.right)
        } else {
            newBounds.origin.x += padding.left + 50
            newBounds.size.width -= (padding.left + padding.right + 50)
        }
        
        newBounds.origin.y += padding.top
        newBounds.size.height -= (padding.top * 2)
        
        return newBounds
    }
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
    // Drawing code
    }
    */
    
}
