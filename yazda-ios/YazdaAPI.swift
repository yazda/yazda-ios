//
//  YazdaAPI.swift
//  yazda-ios
//
//  Created by James Stoup on 3/13/15.
//  Copyright (c) 2015 yazda. All rights reserved.
//

import Foundation

public class YazdaAPI: NSObject {
    lazy var session:YazdaSession = { return YazdaSession() }()
    lazy var adventures:YazdaEvent = { return YazdaEvent() }()
    lazy var users:YazdaUser = { return YazdaUser() }()
    lazy var friends:YazdaFriends = { return YazdaFriends() }()
    lazy var followers:YazdaFollowers = { return YazdaFollowers() }()
    lazy var friendships:YazdaFriendships = { return YazdaFriendships() }()
    lazy var invites:YazdaInvites = { return YazdaInvites() }()
    lazy var account:YazdaAccountSettings = { return YazdaAccountSettings() }()
    lazy var password:YazdaPassword = { return YazdaPassword() }()
    lazy var device:YazdaDevice = { return YazdaDevice() }()
    
    override init() {
        YazdaAPI.Contstants.client.authorizeClientCredentials({ () -> Void in
            NSLog("Authorized Client Credentials")
        }, onFailure: { (failure) -> () in
            NSLog("Failed to Authorize Client Credentials")
        })
        
        super.init()
    }
    
    func isLoggedIn() -> Bool
    {
        return YazdaAPI.Contstants.client.isLoggedIn()
    }
    
    struct Contstants {
        static let VERSION = "v0.1"
        static var client = YazdaOAuthClient()
    }
}