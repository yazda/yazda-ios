//
//  ResetPasswordWithTokenViewController.swift
//  yazda-ios
//
//  Created by James Stoup on 4/5/15.
//  Copyright (c) 2015 yazda. All rights reserved.
//

import UIKit
import AFNetworking

class ResetPasswordWithTokenViewController: BaseViewController {
    var token:String = ""
    
    @IBOutlet weak var password: YazdaTextField!
    @IBOutlet weak var passwordConfirmation: YazdaTextField!
    
    
    @IBAction func changePassword(sender: AnyObject) {
        var passwordText = self.password.text
        var passwordConfirmationText = self.passwordConfirmation.text
        
        self.disableActions()
        
        client?.password.update(self.token, password: passwordText, passwordConfirmation: passwordConfirmation.text, success: { (data) -> Void in
            self.enableActions()
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("LoginViewController") as! LoginViewController
            
            self.presentViewController(controller, animated: true, completion: nil)

            //TODO log them in or force them to relogin?
            }, failure: { (op: AFHTTPRequestOperation, error: NSError) -> Void in
                if let obj = op.responseObject as? NSDictionary {
                    let messages = obj.objectForKey("errors") as! [String]
                    
                    YazdaAlert(parentView: self, messages: messages, withDelay: Common.Interval)
                }
                self.enableActions()
        })
        
        self.password.resignFirstResponder()
        self.passwordConfirmation.resignFirstResponder()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.password.delegate = self
        self.passwordConfirmation.delegate = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
