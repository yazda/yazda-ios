//
//  SetLocationViewController.swift
//  yazda-ios
//
//  Created by James Stoup on 3/16/15.
//  Copyright (c) 2015 yazda. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import FontAwesomeIconFactory

class SetLocationViewController: UIViewController, MKMapViewDelegate, UISearchBarDelegate {
    var delegate:SetLocationViewDelegate?
    
    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var fixItView: UIView!
    
    let locman = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.fixItView.backgroundColor = Common.yazdaGreen
        self.searchBar.barTintColor = Common.yazdaGreen
        self.searchBar.translucent = true
        
        var createPoint = UITapGestureRecognizer(target: self, action: "atLocation:")
        
        map.addGestureRecognizer(createPoint)
    }
    
    func mapView(mapView: MKMapView!, viewForAnnotation annotation: MKAnnotation!) -> MKAnnotationView! {
        
        var annView = mapView.dequeueReusableAnnotationViewWithIdentifier("test")
        
        if annView == nil
        {
            annView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "test")
            
            var button:UIButton = UIButton(frame: CGRectMake(0,0,45,45))
            var factory = NIKFontAwesomeIconFactory.buttonIconFactory()
            
            factory.colors = [UIColor.whiteColor()]
            factory.size = 25
            button.backgroundColor = Common.yazdaOrange
            button.setImage(factory.createImageForIcon(NIKFontAwesomeIcon.PlusCircle), forState: UIControlState.Normal)
            button.imageView?.contentMode = UIViewContentMode.Center
            
            button.addTarget(self, action: "setLocation", forControlEvents: UIControlEvents.TouchUpInside)
            
            annView.canShowCallout = true
            annView.leftCalloutAccessoryView = button
        }
        
        return annView
    }
    
    func atLocation(gestureRecognizer: UITapGestureRecognizer)
    {
        var touchPoint = gestureRecognizer.locationInView(self.map)
        var newCoordinate:CLLocationCoordinate2D = map.convertPoint(touchPoint, toCoordinateFromView: self.map)
        
        let geo = CLGeocoder()
        
        var location = CLLocation(latitude: newCoordinate.latitude, longitude: newCoordinate.longitude)
        
        geo.reverseGeocodeLocation(location) { (address, error) -> Void in
            let mark = address[0] as! CLPlacemark
            
            self.setAnnotation(mark)
        }
    }
    
    func setLocation()
    {
        let annotation = self.map.annotations[0] as! MKAnnotation
        
        println(annotation.coordinate)
        println(annotation.title)
        
        self.delegate?.setLocationViewDelegateSelectLoction(annotation.coordinate, address: annotation.title)
        self.dismissViewControllerAnimated(true, completion: { () -> Void in })
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        let s = searchBar.text
        if s == nil || count(s) < 5 { return }
        let geo = CLGeocoder()
        
        geo.geocodeAddressString(s) {
            (placemarks : [AnyObject]!, error : NSError!) in
            if nil == placemarks {
                println(error.localizedDescription)
                return
            }
            self.map!.showsUserLocation = false
            let p = placemarks[0] as! CLPlacemark
            
            self.setAnnotation(p)
        }
    }
    
    func mapView(mapView: MKMapView!, rendererForOverlay overlay: MKOverlay!) -> MKOverlayRenderer! {
        var v : MKPolylineRenderer! = nil
        if let overlay = overlay as? MKPolyline {
            v = MKPolylineRenderer(polyline:overlay)
            v.strokeColor = UIColor.blueColor().colorWithAlphaComponent(0.8)
            v.lineWidth = 2
        }
        return v
    }
    
    func setAnnotation(p: CLPlacemark)
    {
        let mp = MKPlacemark(placemark:p)
        self.map!.setRegion(
            MKCoordinateRegionMakeWithDistance(mp.coordinate, 1000, 1000),
            animated: true)
        self.map!.removeAnnotations(self.map.annotations)
        self.map!.addAnnotation(mp)
        self.map!.selectAnnotation(mp, animated: true)
    }
}