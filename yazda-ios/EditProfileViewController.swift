//
//  EditProfileViewController.swift
//  yazda-ios
//
//  Created by James Stoup on 4/10/15.
//  Copyright (c) 2015 yazda. All rights reserved.
//

import UIKit
import FontAwesome
import FontAwesomeIconFactory
import AFNetworking

class EditProfileViewController: BaseViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    var user:NSDictionary?
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var uploadPhoto: UIButton!
    @IBOutlet weak var nameText: YazdaTextField!
    @IBOutlet weak var locationText: YazdaTextField!
    @IBOutlet weak var bioText: YazdaTextView!
    @IBOutlet weak var bikePreference: UISwitch!
    @IBOutlet weak var hikePreference: UISwitch!
    
    @IBAction func showPhotoActionSheet(sender: AnyObject) {
        let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        var imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        
        let cameraAction = UIAlertAction(title: "Take Photo", style: UIAlertActionStyle.Default) { (alert: UIAlertAction!) -> Void in
            imagePickerController.sourceType = UIImagePickerControllerSourceType.Camera
            
            self.presentViewController(imagePickerController, animated: true, completion: { () -> Void in
            })
        }
        
        let libraryAction = UIAlertAction(title: "Photo Library", style: UIAlertActionStyle.Default) { (alert: UIAlertAction!) -> Void in
            imagePickerController.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
            
            self.presentViewController(imagePickerController, animated: true, completion: { () -> Void in
            })
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel) { (alert: UIAlertAction!) -> Void in
        }
        
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)) {
            optionMenu.addAction(cameraAction)
        }
        
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary)) {
            optionMenu.addAction(libraryAction)
        }
        
        optionMenu.addAction(cancelAction)
        
        self.presentViewController(optionMenu, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.nameText.delegate = self
        self.locationText.delegate = self
        self.bioText.delegate = self
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        // Do any additional setup after loading the view.
        var url = self.user!.objectForKey("profile_image_url") as! String
        var nsURL = NSURL(string: url)
        var data = NSData(contentsOfURL: nsURL!)
        var image = UIImage(data: data!)

        self.uploadPhoto.setBackgroundImage(image, forState: UIControlState.Normal)
        self.uploadPhoto.layer.cornerRadius = self.uploadPhoto.frame.size.height/2
        self.uploadPhoto.clipsToBounds = true
        self.uploadPhoto.layer.borderWidth = 2.0
        self.uploadPhoto.layer.borderColor = Common.yazdaOrange.CGColor
        
        var factory = NIKFontAwesomeIconFactory.buttonIconFactory()
        factory.size = 25
        self.nameText.addIcon(NIKFontAwesomeIcon.Question)
        self.locationText.addIcon(NIKFontAwesomeIcon.MapMarker)
        
        self.nameText.delegate = self
        self.locationText.delegate = self
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: UIBarButtonItemStyle.Done, target: self, action: "updateProfile")
        
        self.nameText.text = self.user!.objectForKey("name") as! String
        self.locationText.text = self.user!.objectForKey("location") as? String
        self.bioText.text = self.user!.objectForKey("description") as? String
        self.bikePreference.on = self.user!.objectForKey("views_mountain_biking") as! Bool
        self.hikePreference.on = self.user!.objectForKey("views_hiking") as! Bool
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateProfile() {
        self.disableActions()
        
        let name = self.nameText.text
        let location = self.locationText.text
        let description = self.bioText.text
        let viewsMountainBiking = self.bikePreference.on
        let viewsHiking = self.hikePreference.on
        
        client?.account.update(nil, phone: nil, newsletter: nil, name: name, location: location, description: description, notifiesInvite: nil, notifiesFriendRequest: nil, viewsMountainBiking: viewsMountainBiking, viewsHiking: viewsHiking, success: { (obj) -> Void in
            
            self.enableActions()
            self.navigationController?.popViewControllerAnimated(true)
            
            }, failure: { (op: AFHTTPRequestOperation, error: NSError) -> Void in
                if let obj = op.responseObject as? NSDictionary {
                    let messages = obj.objectForKey("errors") as! [String]
                    
                    YazdaAlert(parentView: self, messages: messages, withDelay: Common.Interval)
                }
                
                self.enableActions()
        })
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!) {
        //TODO upload photo or resize
        self.disableActions()
        
        var imageSize = CGImageGetHeight(image.CGImage) * CGImageGetBytesPerRow(image.CGImage)
        var theImage = UIImagePNGRepresentation(image)
        
        if(imageSize > 700000) {
            let bounds = CGSize(width: 250, height: 250)
            let aspectRatio = image.size.width / image.size.height
            let newImageWidth = aspectRatio * bounds.height
            let newSize = CGSize(width: newImageWidth, height: bounds.height)
            
            // Create a new image context and draw the image into the "bounds" instead of "newSize" as was done in the case of the Aspect Fit method.
            UIGraphicsBeginImageContextWithOptions(bounds, true, 0)
            
            let xCoord = -(newImageWidth - bounds.width) / 2
            image.drawInRect(CGRect(origin: CGPoint(x:xCoord, y:0), size: newSize))
            
            theImage = UIImagePNGRepresentation(UIGraphicsGetImageFromCurrentImageContext())
            UIGraphicsEndImageContext()
        }
        
        picker.dismissViewControllerAnimated(true, completion: nil)
        
        client?.account.updateProfileImage(theImage, success: { (obj) -> Void in
            self.enableActions()
            
            let lastIndex = self.navigationController!.viewControllers.count - 1
            let controller = self.navigationController?.viewControllers[0] as! ProfileViewController
            
            controller.getUserProfile()
            self.navigationController?.popViewControllerAnimated(true)
            
            }, failure: { (op: AFHTTPRequestOperation, error: NSError) -> Void in
                if let obj = op.responseObject as? NSDictionary {
                    let messages = obj.objectForKey("errors") as! [String]
                    
                    YazdaAlert(parentView: self, messages: messages, withDelay: Common.Interval)
                }
                
                self.enableActions()
        })
        
    }
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
