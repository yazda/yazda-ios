//
//  YazdaFollowers.swift
//  yazda-ios
//
//  Created by James Stoup on 3/14/15.
//  Copyright (c) 2015 yazda. All rights reserved.
//

import Foundation
import AFNetworking

class YazdaFollowers: YazdaBaseService {
    let BASE_URL = "/users"
    
    func index(userId: Int!, page: Int?, pageSize: Int?, success: (AnyObject!)->Void, failure: (AFHTTPRequestOperation, NSError)->Void)
    {
        var params = NSMutableDictionary(dictionary: [String : AnyObject]())
        var url = buildURL(BASE_URL, params: String(userId), "followers")
        
        addParamUnlessNil(&params, param: page, forKey: "page")
        addParamUnlessNil(&params, param: pageSize, forKey: "page_size")
        
        YazdaAPI.Contstants.client.GET(url, parameters: params, retry: true, success: { (op: AFHTTPRequestOperation!, object: AnyObject!) -> Void in
            
            NSLog("Getting followers with id: %0d and params: %@", userId, params)
            
            success(object)
            
            }) { (op: AFHTTPRequestOperation!, error: NSError!) -> Void in
                
                NSLog("Failed getting followers id: %0d with params: %@", userId,  params)
                
                failure(op, error)
        }
    }
}