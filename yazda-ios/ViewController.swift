//
//  ViewController.swift
//  yazda-ios
//
//  Created by James Stoup on 3/12/15.
//  Copyright (c) 2015 yazda. All rights reserved.
//

import UIKit
import AFNetworking

class ViewController: BaseViewController {
    
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    
    @IBAction func register(sender: AnyObject) {
        let nameText = name.text
        let emailText = email.text
        let passwordText = password.text
        
        self.disableActions()
        
        client!.session.register(emailText, password: passwordText, name: nameText, success: { (data: AnyObject!) -> Void in
            
            self.enableActions()
            
            }) { (op: AFHTTPRequestOperation, error: NSError) -> Void in
                if let obj = op.responseObject as? NSDictionary {
                    let messages = obj.objectForKey("errors") as! [String]
                    
                    YazdaAlert(parentView: self, messages: messages, withDelay: Common.Interval)
                }
                self.enableActions()
        }
        
        self.name.resignFirstResponder()
        self.email.resignFirstResponder()
        self.password.resignFirstResponder()
    }
    
    override func viewDidLoad() {
        // Do any additional setup after loading the view, typically from a nib.
        
        self.name.delegate = self
        self.email.delegate = self
        self.password.delegate = self
        
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

