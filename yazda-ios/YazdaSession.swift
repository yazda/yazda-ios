//
//  YazdaRegister.swift
//  yazda-ios
//
//  Created by James Stoup on 3/13/15.
//  Copyright (c) 2015 yazda. All rights reserved.
//

import Foundation
import AFNetworking

class YazdaSession: YazdaBaseService {
    let BASE_URL = "/register"
    
    func register(email: String!, password: String!, name: String!, success: (AnyObject!)->Void, failure: (AFHTTPRequestOperation, NSError)->Void) {
        var url = buildURL(BASE_URL, params: "")
        var params = NSMutableDictionary(dictionary: [String : AnyObject]())
        
        addParamUnlessNil(&params, param: email, forKey: "email")
        addParamUnlessNil(&params, param: password, forKey: "password")
        addParamUnlessNil(&params, param: password, forKey: "password_confirmation")
        addParamUnlessNil(&params, param: name, forKey: "name")
        addParamUnlessNil(&params, param: true, forKey: "newsletter")
        
        YazdaAPI.Contstants.client.POST(url, parameters: params, retry: true, success: { (op: AFHTTPRequestOperation!, object: AnyObject!) -> Void in
            
            NSLog("Registration Successful")
            
            self.login(email, password: password, success: { () -> Void in
                Common.navigateToLoggedInStoryboard()
                }, failure: { (errorMessage: NSString!) -> Void in
                    //TODO error message
            })
            
            }) { (op: AFHTTPRequestOperation!, error: NSError!) -> Void in
                println(error)
                failure(op, error)
        }
    }
    
    func login(email: String!, password: String!, success: ()-> Void, failure onFailure: (NSString!)->Void)
    {
        YazdaAPI.Contstants.client.authorizeUser(email, password: password, onSuccess: { () -> (Void) in
            NSLog("Authorized User Credentials")
            
            Common.registerForNotifications()
            
            success()
            
            }, onFailure: { (failure) -> () in
                onFailure(failure)
                NSLog("Failed to Authorize User Credentials")
        })
    }
    
    func logout()
    {
        YazdaAPI.Contstants.client.resignAuthorization()
        YazdaAPI.Contstants.client.authorizeClientCredentials({ () -> Void in
            NSLog("Authorized Client Credentials")
            }, onFailure: { (failure) -> () in
                NSLog("Failed to Authorize Client Credentials")
        })
    }
}