//
//  EventViewController.swift
//  yazda-ios
//
//  Created by James Stoup on 3/15/15.
//  Copyright (c) 2015 yazda. All rights reserved.
//

import UIKit
import AFNetworking
import EventKit
import MapKit
import FontAwesomeIconFactory

class EventViewController: BaseViewController {
    var adventureId:Int?
    var event:NSDictionary?
    
    //TODO handle scroll view
    
    @IBOutlet weak var eventName: UILabel!
    @IBOutlet weak var eventDescription: UITextView!
    @IBOutlet weak var eventTypeLabel: UILabel!
    @IBOutlet weak var eventTime: UIButton!
    @IBOutlet weak var eventLocation: UIButton!
    @IBOutlet weak var numberOfAttendees: UILabel!
    @IBOutlet weak var yaNaButton: UISegmentedControl!
    @IBOutlet weak var eventTypeImage: UILabel!
    @IBOutlet weak var descriptionHeight: NSLayoutConstraint!
    @IBOutlet weak var yaNaHeight: NSLayoutConstraint!
    
    var friendCollectionView: FriendCollectionViewDelegate! = nil
    
    @IBAction func addToCalendar(sender: AnyObject) {
        var eventStore:EKEventStore = EKEventStore()
        
        eventStore.requestAccessToEntityType(EKEntityTypeEvent, completion: { (granted, error) -> Void in
            if (granted) && (error == nil) {
                let theStartTime = Common.toDate(self.event?.objectForKey("start_time") as! String)
                let theEndTime = Common.toDate(self.event?.objectForKey("end_time") as! String)
                
                var event:EKEvent = EKEvent(eventStore: eventStore)
                
                event.title = self.eventName.text!
                event.startDate = theStartTime
                event.endDate = theEndTime
                event.location = self.event?.objectForKey("address") as? String
                event.notes = self.event?.objectForKey("description") as? String
                event.calendar = eventStore.defaultCalendarForNewEvents
                
                eventStore.saveEvent(event, span: EKSpanThisEvent, error: nil)
                
                var alert = UIAlertController(title: "Added to calendar", message: "This Adventure has been added to your calendar", preferredStyle: UIAlertControllerStyle.Alert)
                
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action: UIAlertAction!) -> Void in
                    
                    alert.dismissViewControllerAnimated(true, completion: nil)
                }))
                
                self.presentViewController(alert, animated: true, completion: nil)
            }
        })
    }
    
    @IBAction func openInMaps(sender: AnyObject) {
        let lat = event?.objectForKey("lat") as? CLLocationDegrees
        let lon = event?.objectForKey("lon") as? CLLocationDegrees
        let regionDistance:CLLocationDistance = 10000
        
        var coordinates = CLLocationCoordinate2DMake(lat!, lon!)
        
        let regionSpan = MKCoordinateRegionMakeWithDistance(coordinates, regionDistance, regionDistance)
        var options = [ MKLaunchOptionsMapCenterKey: NSValue(MKCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(MKCoordinateSpan: regionSpan.span)]
        
        var placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        var mapItem = MKMapItem(placemark: placemark)
        mapItem.name = "\(self.eventName.text!)"
        mapItem.openInMapsWithLaunchOptions(options)
    }
    
    @IBAction func respondToEvent(sender: UISegmentedControl) {
        let adventureId = event?.objectForKey("id") as! Int
        
        self.disableActions()
        
        if(sender.selectedSegmentIndex == 0) {
            client?.invites.accept(adventureId, success: { (data) -> Void in
                self.enableActions()
                
                }, failure: { (op: AFHTTPRequestOperation, error: NSError) -> Void in
                    if let obj = op.responseObject as? NSDictionary {
                        let messages = obj.objectForKey("errors") as! [String]
                        
                        YazdaAlert(parentView: self, messages: messages, withDelay: Common.Interval)
                    }
                    self.enableActions()
            })
        } else {
            client?.invites.reject(adventureId, success: { (data) -> Void in
                self.enableActions()
                }, failure: { (op: AFHTTPRequestOperation, error: NSError) -> Void in
                    if let obj = op.responseObject as? NSDictionary {
                        let messages = obj.objectForKey("errors") as! [String]
                        
                        YazdaAlert(parentView: self, messages: messages, withDelay: Common.Interval)
                    }
                    self.enableActions()
            })
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.adventureId != nil {
            client?.adventures.show(adventureId!, success: { (obj) -> Void in
                let adventure = obj as! NSDictionary
                
                self.event = adventure.objectForKey("adventure") as! NSDictionary
                
                self.setAdventure()
                }, failure: { (op: AFHTTPRequestOperation, error: NSError) -> Void in
                    //TODO
            })
            
        } else {
            self.setAdventure()
        }
    }
    
    func setAdventure() {
        let name = event?.objectForKey("name") as? String
        let attending = event?.objectForKey("is_attending") as? Bool
        let invited = event?.objectForKey("is_invited") as! Bool
        let hasResponded = event?.objectForKey("has_responded") as! Bool
        let isOwner = event?.objectForKey("is_owner") as! Bool
        let theDescription = event?.objectForKey("description") as? String
        let theAddress = event?.objectForKey("address") as? String
        let theEventType = event?.objectForKey("adventure_type") as? String
        let theStartTime = Common.formatDateAndTime(event?.objectForKey("start_time") as! String)
        let theEndTime = Common.formatDateAndTime(event?.objectForKey("end_time") as! String)
        let startTimeDate = Common.toDate(event?.objectForKey("start_time") as! String)
        let now = NSDate()
        
        if now.compare(startTimeDate) == NSComparisonResult.OrderedDescending || isOwner {
            self.yaNaButton.enabled = false
            self.yaNaButton.hidden = true
            self.yaNaHeight.constant = 0
        }
        
        if(hasResponded == true && attending == true)
        {
            self.yaNaButton.selectedSegmentIndex = 0
        } else if(hasResponded == true && attending == false) {
            self.yaNaButton.selectedSegmentIndex = 1
        }
        
        //TODO we need to request the object as there aren't any user's from invite screen
        let invites = event?.objectForKey("attendings") as? NSArray
        
        self.friendCollectionView!.friendCollectionViewDelegateAddFriend(event?.objectForKey("owner") as! NSDictionary)
        
        for invite in invites!
        {
            let i = invite as! NSDictionary
            let user = i.objectForKey("user") as! NSDictionary
            
            self.friendCollectionView!.friendCollectionViewDelegateAddFriend(user)
        }
        
        self.eventLocation.setTitle(theAddress, forState: UIControlState.Normal)
        self.title = name
        self.eventName.text = name
        self.numberOfAttendees.text = "\(invites!.count + 1) Adventurers going"
        self.eventTime.setTitle("\(theStartTime) to \(theEndTime)", forState: UIControlState.Normal)
        
        if(theDescription != nil && theDescription != "") {
            self.eventDescription.text = theDescription
            self.eventDescription.editable = false
            self.eventDescription.dataDetectorTypes = UIDataDetectorTypes.All
        } else {
            self.descriptionHeight.constant = 0.0
        }
        
        if(theEventType == "hiking") {
            self.eventTypeImage.font = UIFont(name: "icomoon", size: 20)
            self.eventTypeImage.text = Common.HIKING
            
            self.eventTypeLabel.text = "HIKING"
        } else {
            self.eventTypeImage.font = UIFont(name: "FontAwesome", size: 20)
            self.eventTypeImage.text = Common.MOUNTAIN_BIKING
            
            self.eventTypeLabel.text = "MOUNTAIN BIKING"
        }
        self.eventDescription.sizeToFit()
        
        // TODO move this elsewhere later
        if isOwner {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Cancel Adventure", style: UIBarButtonItemStyle.Plain, target: self, action: "cancelAdventure")
            
        }
    }
    
    func cancelAdventure() {
        var alert = UIAlertController(title: "Cancel Adventure", message: "Are you sure you want to cancel this adventure?  All invitees will be notified that the adventure will not take place.", preferredStyle: UIAlertControllerStyle.Alert)
        
        var cancelAction = UIAlertAction(title: "No", style: UIAlertActionStyle.Cancel) { (action) -> Void in
            alert.dismissViewControllerAnimated(true, completion: nil)
        }
        
        var cancelAdventure = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Destructive) { (action) -> Void in
            self.disableActions()
            
            client?.adventures.cancel(self.event?.objectForKey("id") as! Int, success: { (obj) -> Void in
                
                self.enableActions()
                alert.dismissViewControllerAnimated(true, completion: nil)
                self.navigationController?.popViewControllerAnimated(true)
                
                }, failure: { (op: AFHTTPRequestOperation, error: NSError) -> Void in
                    if let obj = op.responseObject as? NSDictionary {
                        let messages = obj.objectForKey("errors") as! [String]
                        
                        YazdaAlert(parentView: self, messages: messages, withDelay: Common.Interval)
                    }
                    
                    alert.dismissViewControllerAnimated(true, completion: nil)
                    self.enableActions()
            })
        }
        
        alert.addAction(cancelAction)
        alert.addAction(cancelAdventure)
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if(segue.identifier == "eventFriendList") {
            self.friendCollectionView = segue.destinationViewController as! FriendCollectionViewController
        }
    }
}
