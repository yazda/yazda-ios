//
//  AppDelegate.swift
//  yazda-ios
//
//  Created by James Stoup on 3/12/15.
//  Copyright (c) 2015 yazda. All rights reserved.
//

import UIKit
import AFNetworking

var client:YazdaAPI?
var badgeNumber:Int = 0
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        
        Common.setupUILookAndFeel()
        
        NewRelic.startWithApplicationToken("AA306fec7267794c7ab273fce5b0928c2b286eac42")
        client = YazdaAPI()
        
        if(client!.isLoggedIn()) {
            Common.registerForNotifications()
            
            let storyboard = UIStoryboard(name: "LoggedIn", bundle: nil)
            let loggedInController = storyboard.instantiateViewControllerWithIdentifier("YazdaTabBarController") as! UITabBarController
            window?.rootViewController = loggedInController
        }
        
        var remoteOptions = launchOptions?[UIApplicationLaunchOptionsRemoteNotificationKey] as? [NSObject: AnyObject]
        
        if (remoteOptions != nil) {

            self.application(application, didReceiveRemoteNotification: remoteOptions!, fetchCompletionHandler: { (UIBackgroundFetchResult) -> Void in
            })
        }
        
        return true
    }
    
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject?) -> Bool {
        var host = url.host!
        
        if(host == "reset_password")
        {
            let query = url.query!.componentsSeparatedByString("=")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("ResetPasswordWithTokenController") as! ResetPasswordWithTokenViewController
            
            controller.token = query[1]
            window?.rootViewController = controller
            
            return true
        } else if (client!.isLoggedIn()) {
            if(host == "settings") {
                let storyboard = UIStoryboard(name: "LoggedIn", bundle: nil)
                let controller = storyboard.instantiateViewControllerWithIdentifier("YazdaTabBarController") as! YazdaTabBarViewController
                
                controller.selectedIndex = 3
                
                window?.rootViewController = controller
                
                return true
            } else if (host == "adventures") {
                let query = url.query!.componentsSeparatedByString("=")
                let storyboard = UIStoryboard(name: "LoggedIn", bundle: nil)
                let id = query[1] as NSString
                let tab = self.window?.rootViewController as! YazdaTabBarViewController
                let viewControllers = tab.viewControllers as! NSArray
                
                var adventureController = storyboard.instantiateViewControllerWithIdentifier("adventureViewController") as! EventViewController
                var nav = viewControllers.objectAtIndex(0) as! UINavigationController
                
                adventureController.adventureId = id.integerValue
                nav.pushViewController(adventureController, animated: true)
                
                return true
            }
        } else {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("LoginViewController") as! LoginViewController
            
            window?.rootViewController = controller
            
            return true
        }
        
        return false
    }
    
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
    }
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        var tokenString = deviceToken.description.stringByTrimmingCharactersInSet(NSCharacterSet(charactersInString: "<>")).stringByReplacingOccurrencesOfString(" ", withString: "", options: nil, range: nil)
        
        client?.device.create(UIDevice.currentDevice().name, token: tokenString, success: { (obj) -> Void in
            //
            }, failure: { (op: AFHTTPRequestOperation, error: NSError) -> Void in
                //TODO failure do nothing?
                
        })
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject], fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
        badgeNumber = UIApplication.sharedApplication().applicationIconBadgeNumber + 1
        
        NSNotificationCenter.defaultCenter().postNotificationName("inviteNotification", object: nil, userInfo: userInfo)
        
        completionHandler(UIBackgroundFetchResult.NewData)
    }
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
}

