//
//  BackgroundTask.swift
//  yazda-ios
//
//  Created by James Stoup on 3/21/15.
//  Copyright (c) 2015 yazda. All rights reserved.
//

import Foundation
import UIKit

class BackgroundTask {
    class func downloadUserProfileImage(inout imageView:UIImageView!, url: NSURL!) {
        var imageQueue = dispatch_queue_create("com.yazda.yazda-ios.imagequeue", nil)
        
        dispatch_async(imageQueue, {
            () in
            
            var imageData = NSData(contentsOfURL: url)
            
            if imageData != nil {
                var imageLoad:UIImage? = UIImage(data: imageData!)
                
                dispatch_async(dispatch_get_main_queue(), {
                    () in
                    
                    if imageView != nil {
                        imageView.image = imageLoad
                        imageView.layer.cornerRadius = imageView.frame.size.height/2
                        imageView.clipsToBounds = true
                    }
                })
            }
        })
    }
}