//
//  BaseTableViewController.swift
//  yazda-ios
//
//  Created by James Stoup on 7/21/15.
//  Copyright (c) 2015 yazda. All rights reserved.
//

import UIKit

class BaseTableViewController: UITableViewController {
    private var refresher:UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        refresher = UIRefreshControl()
        refresher.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refresher.addTarget(self, action: "refresh", forControlEvents: UIControlEvents.ValueChanged)
        self.tableView.addSubview(refresher)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func disableActions() {
        self.refresher.beginRefreshing()
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
    }
    
    func enableAction() {
        self.refresher.endRefreshing()
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
    }
    
}
