//
//  YazdaTabBarViewController.swift
//  yazda-ios
//
//  Created by James Stoup on 6/30/15.
//  Copyright (c) 2015 yazda. All rights reserved.
//

import UIKit

class YazdaTabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "updateBadge:", name: "inviteNotification", object: nil)
        
        updateBadge(self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateBadge(selector: AnyObject!) {
        var item = self.tabBar.items?[2] as! UITabBarItem
        
        UIApplication.sharedApplication().applicationIconBadgeNumber = badgeNumber
        
        if badgeNumber == 0 {
            item.badgeValue = nil
        } else {
            item.badgeValue = badgeNumber.description
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
