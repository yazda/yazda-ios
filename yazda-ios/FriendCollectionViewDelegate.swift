//
//  FriendCollectionViewDelegate.swift
//  yazda-ios
//
//  Created by James Stoup on 3/23/15.
//  Copyright (c) 2015 yazda. All rights reserved.
//

import Foundation

protocol FriendCollectionViewDelegate {
    func friendCollectionViewDelegateAddFriend(user:NSDictionary!) -> Void
    func friendCollectionViewDelegateRemoveFriend(user:NSDictionary!) -> Void
}